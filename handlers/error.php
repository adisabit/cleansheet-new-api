<?php

use Skeleton\Core\Library\Response;

Flight::map('error', function($e) {
    Response::error($e, $e->getCode());
});