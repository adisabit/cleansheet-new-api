<?php

use Skeleton\Core\Library\Response;
use Skeleton\Core\Exception\HttpException;

Flight::map('notFound', function() {
    Response::error(new HttpException('API tidak ditemukan!', 404));
});