<?php

use Cleansheet\Config;
use Skeleton\Core\Library\Response;
use Skeleton\Core\Exception\HttpException;

include __DIR__ . '/vendor/autoload.php';

if (in_array(Config::$environment, ['local', 'development']) && file_exists(__DIR__ .'/.env')) {
    $dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
}

// set default timezone
date_default_timezone_set("Asia/Jakarta");

// load config
include __DIR__ . '/config/index.php';

// load middleware
include __DIR__ . '/middleware/cors.php';
include __DIR__ . '/middleware/auth.php';

// load routes
include __DIR__ . '/routes/index.php';

// load handler
include __DIR__ . '/handlers/notfound.php';
include __DIR__ . '/handlers/error.php';

Flight::start();