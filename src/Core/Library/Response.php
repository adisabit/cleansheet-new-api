<?php

namespace Skeleton\Core\Library;

use Exception;
use Skeleton\Core\Exception\HttpException;
use Skeleton\Core\System\Log\Service as LogService;

class Response
{
    private static $data = NULL;
    private static $error = NULL;

    private static $exception = NULL;
    private static $exceptionCode = NULL;
    private static $exceptionMessage = NULL;
    private static $exceptionTrace = NULL;

    private static function render($code, $status)
    {
        $serverMemoryUsage = memory_get_usage() / 1024;

        $response = [
            'code' => $code,
            'success' => $status,
            'data' => self::$data,
            'errors' => self::$error,
            'metadata' => [
                'elapsed_time'          => number_format(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], 5),
                'server_memory_usage'   => number_format($serverMemoryUsage) . ' KB'
            ]
        ];


        $logService = new LogService();
        $logData = [
            'base_url'              => Request::getBaseUrl(),
            'request_url'           => Request::getUrl(),
            'request_method'        => Request::getMethod(),
            'request_header'        => json_encode(Request::getHeaders()),
            'request_body'          => json_encode(Request::getBody()),
            'request_time'          => date("Y-m-d H:i:s", $_SERVER['REQUEST_TIME']),
            'server_memory_usage'   => $serverMemoryUsage,
            'response_code'         => $code,
            'response_body'         => json_encode($response),
            'response_time'         => date("Y-m-d H:i:s"),
            'error_type'            => self::$exception,
            'error_code'            => self::$exceptionCode,
            'error_message'         => self::$exceptionMessage,
            'error_trace'           => self::$exceptionTrace
        ];

        $log = $logService->insert($logData);

        if (!empty(self::$error)) {
            $response['errors'] = [
                'log_id' => $log['id'],
                'message' => self::$error
            ];
        }

        header("Content-Type: application/json");

        echo json_encode($response);
    }

    public static function success($data, int $code = 200)
    {
        self::$data = $data;
        
        self::render($code, true);
    }

    public static function error($e)
    {
        $code = $e instanceof HttpException ? $e->getCode(): 500;
        self::$exceptionCode = $e->getCode();
        self::$exceptionTrace = $e->getTraceAsString();
        self::$error = $e instanceof HttpException ? $e->getMessage() : 'Terjadi kesalahan. Harap coba beberapa saat lagi!';
        
        do {
            $exception = $e;
            $e = $e->getPrevious();
        } while ( $e != NULL );
        
        self::$exception = get_class($exception);
        self::$exceptionMessage = $exception->getMessage();
        self::render($code, false);
    }
}