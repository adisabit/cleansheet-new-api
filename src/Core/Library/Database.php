<?php

namespace Skeleton\Core\Library;

use PDO;
use PDOException;
use Skeleton\Core\Exception\DatabaseConfigNotFoundException;

class Database
{
    public static $databases = [];

    private $hasActiveTransaction = false;
    private static $connections = [];

    private $connection;

    /**
     * Database constructor
     *
     * @param string $databaseName
     * @param PDO $pdo
     */
    public function __construct(string $databaseName, PDO $pdo = NULL)
    {
        $this->connection = Database::init($databaseName, $pdo);
    }

    /**
     * Init instance
     *
     * @param string $databaseName
     * @param PDO $pdo
     * @return PDO
     */
    private static function init(string $databaseName, PDO $pdo = NULL): PDO
    {
        if ( !isset(self::$connections[$databaseName]) ) {
            if (!array_key_exists($databaseName, self::$databases)) {
                throw new DatabaseConfigNotFoundException("Database configuration for $databaseName is not exist!", 500);
            }

            if ($pdo != NULL) {
                self::$connections[$databaseName] = $pdo;
            } else {
                $instance = parse_url(self::$databases[$databaseName]);
                $dsn = sprintf("%s:host=%s;port=%s;dbname=%s", $instance['scheme'], $instance['host'], $instance['port'], str_replace('/', '', $instance['path']));
                
                self::$connections[$databaseName] = new PDO($dsn, $instance['user'], $instance['pass']); 
            }
        }

        return self::$connections[$databaseName];
    }

    /**
     * Get connection
     *
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->connection;
    }

    /**
     * Begin the transaction
     *
     * @param string $databaseName
     * @return boolean
     */
    public function beginTransaction(): string
    {
        if (!$this->hasActiveTransaction) {
            $this->hasActiveTransaction = $this->connection->beginTransaction();
        }

        return $this->hasActiveTransaction;
    }

    /**
     * Commit transaction
     *
     * @return boolean
     */
    public function commit(): bool
    {
        if ($this->hasActiveTransaction) {
            $this->connection->commit();

            $this->hasActiveTransaction = false;
        }

        return $this->hasActiveTransaction;
    }

    /**
     * Rollback transaction
     *
     * @return boolean
     */
    public function rollBack(): bool
    {
        if ($this->hasActiveTransaction) {
            $this->connection->commit();

            $this->hasActiveTransaction = false;
        }

        return $this->hasActiveTransaction;
    }
}