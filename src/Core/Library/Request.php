<?php

namespace Skeleton\Core\Library;

use Flight;

class Request
{
    /**
     * Get request url
     *
     * @return string
     */
    public static function getUrl(): string
    {
        return Flight::request()->url;
    }

    /**
     * Get headers
     *
     * @return array
     */
    public static function getHeaders(): array
    {
        $headers = [];

        if (function_exists('apache_request_headers')) {
            $headers = apache_request_headers();
            $headers = array_combine(array_map('strtolower', array_keys($headers)), array_values($headers));
        } else if (isset($_SERVER)) {
            $headers = $_SERVER;
        }

        return $headers;
    }

    /**
     * Get request base url
     *
     * @return string
     */
    public static function getBaseUrl(): string
    {
        return Flight::request()->base;
    }

    /**
     * Get request body
     */
    public static function getBody()
    {
        return Flight::request()->getBody();
    }

    /**
     * Get request method
     *
     * @return string
     */
    public static function getMethod(): string
    {
        return Flight::request()->method;
    }

    /**
     * Get request referrer
     *
     * @return string
     */
    public static function getReferrer(): string
    {
        return Flight::request()->referrer;
    }

    /**
     * Get request ip
     *
     * @return string
     */
    public static function getIp(): string
    {
        return Flight::request()->ip;
    }

    /**
     * Get request ajax
     *
     * @return string
     */
    public static function getAjax(): string
    {
        return Flight::request()->ajax;
    }

    /**
     * Get request scheme
     *
     * @return string
     */
    public static function getScheme(): string
    {
        return Flight::request()->scheme;
    }

    /**
     * Get request user agent
     *
     * @return string
     */
    public static function getUserAgent(): string
    {
        return Flight::request()->user_agent;
    }

    /**
     * Get request type
     *
     * @return string
     */
    public static function getType(): string
    {
        return Flight::request()->type;
    }

    /**
     * Get request length
     *
     * @return string
     */
    public static function getLength(): string
    {
        return Flight::request()->length;
    }

    /**
     * Get request query
     */
    public static function getQuery()
    {
        return Flight::request()->query;
    }

    /**
     * Get request data
     */
    public static function getData()
    {
        return Flight::request()->data;
    }

    /**
     * Get request cookies
     */
    public static function getCookies()
    {
        return Flight::request()->cookies;
    }

    /**
     * Get request files
     */
    public static function getFiles()
    {
        return Flight::request()->files;
    }

    /**
     * Check whether request is secure
     */
    public static function isSecure()
    {
        return Flight::request()->secure;
    }

    /**
     * Get request accept
     */
    public static function getAccept()
    {
        return Flight::request()->accept;
    }

    /**
     * Get request proxy ip
     */
    public static function getProxyIp()
    {
        return Flight::request()->proxy_ip;
    }
}