<?php

namespace Skeleton\Core\Library;

use Cleansheet\Config;
use Exception;
use Kreait\Firebase\Factory;
use Skeleton\Core\Exception\HttpException;
use Google\Cloud\Core\Exception\GoogleException;
use Kreait\Firebase\ServiceAccount;

class Storage
{
    private $bucket;

    /**
     * Storage constructor
     * 
     * @param string $bucketName
     */
    public function __construct(string $bucketName = NULL)
    {
        $serviceAccount = ServiceAccount::fromArray(Config::$serviceAccount[Config::$environment]);
        
        $storage = (new Factory())
            ->withServiceAccount($serviceAccount)
            ->createStorage();

        $this->bucket = $bucketName != NULL ? $storage->getBucket($bucketName) : $storage->getBucket();
    }

    /**
     * Get image url
     *
     * @param string $fileName
     * @return string
     */
    public function getImageUrl(string $fileName): string
    {
        $url = "https://firebasestorage.googleapis.com/v0/b/" . rawurlencode(Config::$bucketName[Config::$environment]) . "/o/" . rawurlencode($fileName);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);

        curl_close($ch);

        if ($result === 'FALSE') {
            throw new Exception(sprintf('cURL error occured: %s (%s)', curl_error($ch), curl_errno($ch)));
        }

        $responseJson = json_decode($result, true);

        $url .= '?alt=media&token=' . $responseJson['downloadTokens'];

        return $url;
    }

    /**
     * Upload base64 file
     *
     * @param string $base64
     * @param string $fileName
     * @return array
     */
    public function uploadBase64(string $base64, string $fileName, string $directory = 'public'): array
    {
        // Save decoded base64 image
        $file = base64_decode($base64);

        $fileName = $directory . '/' . $fileName;
        
        try {
            $this->bucket->upload($file, [
                'name' => $fileName,
                'resumable' => true,
            ]);
                        
            $imageUrl = $this->getImageUrl($fileName);

            return [
                'image_url' => $imageUrl
            ];
        } catch (Exception $e) {
            throw new HttpException("Terjadi kesalahan ketika mengunggah file!", 500, $e);
        }
    }

    /**
     * Upload base64 file
     *
     * @param string $base64
     * @param string $fileName
     * @return array
     */
    public function upload($file, string $fileName, string $directory = 'public'): array
    {
        $fileName = $directory . '/' . $fileName;
        
        try {
            $this->bucket->upload($file, [
                'name' => $fileName,
                'resumable' => true,
            ]);
                        
            $imageUrl = $this->getImageUrl($fileName);

            return [
                'image_url' => $imageUrl
            ];
        } catch (Exception $e) {
            throw new HttpException("Terjadi kesalahan ketika mengunggah file!", 500, $e);
        }
    }
}