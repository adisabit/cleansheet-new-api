<?php

namespace Skeleton\Core\Library;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Builder;
use Illuminate\Http\Request;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Skeleton\Core\Exception\HttpException;

class Token
{
    private static $issuer = '';
    private static $audience = '';

    public static function generate(array $claims): array
    {
        $builder = (new Builder())
            // Configures the issuer (iss claim)
            ->issuedBy(self::$issuer)
            // Configures the audience (aud claim)
            ->permittedFor(self::$audience)
            // Configures the time that the token was issue (iat claim)
            ->issuedAt(time())
            // Configures the expiration time of the token (exp claim)
            ->expiresAt(time() + Environment::get('JWT_EXPIRY_TIME', 3600));

        // Configures claims
        foreach ($claims as $key => $value) {
            $builder->set($key, $value);
        }

        $refreshToken = Hash::md5(microtime());
        $builder->set('refresh_token', $refreshToken);

        // sign token so we know that it is not modified after generation
        $token = $builder->getToken(new Sha256(), new Key(getenv('JWT_KEY')));

        return [
            'access_token' => (string)$token,
            'refresh_token' => $refreshToken
        ];
    }

    private static function parse(string $accessToken)
    {
        $validation = new ValidationData();
        $validation->setIssuer(self::$issuer);
        $validation->setAudience(self::$audience);

        $parsedToken = (new Parser())->parse($accessToken);

        return $parsedToken;
    }

    public static function validate(string $accessToken = NULL): bool
    {
        if ($accessToken == NULL) {
            throw new HttpException('Access token tidak boleh null!', 401);
        }

        $validation = new ValidationData();
        $validation->setIssuer(self::$issuer);
        $validation->setAudience(self::$audience);

        $parsedToken = (new Parser())->parse($accessToken);

        if (!$parsedToken->validate($validation)) {
            throw new HttpException('Token tidak valid!', 403);
        }

        if (!$parsedToken->verify(new Sha256(), new Key(getenv('JWT_KEY')))) {
            throw new HttpException('Verifikasi token gagal!', 403);
        }

        return TRUE;
    }

    public static function getBearerToken()
    {
        if (!isset(apache_request_headers()['Authorization'])) {
            throw new HttpException('Authorization header must be set!', 400);
        }
        
        $header = apache_request_headers()['Authorization'];

        // HEADER: Get the access token from the header
        if ($header !== '' && preg_match('/Bearer\s(\S+)/', $header, $matches)) {
            return $matches[1];
        }

        return NULL;
    }
    
    public static function getClaim(string $accessToken, $key)
    {
        $parsedToken = self::parse($accessToken);

        return $parsedToken->getClaim($key);
    }

    public static function isExpired($accessToken)
    {
        $validation = new ValidationData();
        $validation->setIssuer(self::$issuer);
        $validation->setAudience(self::$audience);

        $parsedToken = (new Parser())->parse($accessToken);

        return $parsedToken->isExpired();
    }

    public static function refresh(string $accessToken, string $refreshToken, array $claimFields): array
    {        
        $prevRefreshToken = self::getClaim($accessToken, 'refresh_token');

        if ($prevRefreshToken != $refreshToken) {
            throw new HttpException('Invalid refresh token!', 401);
        }

        if (self::isExpired($accessToken)) {
            $claims = [];
            foreach ($claimFields as $key) {
                $claims[$key] = self::getClaim($accessToken, $key);
            }

            return self::generate($claims);
        }

        return [
            'access_token' => $accessToken,
            'refresh_token' => $refreshToken
        ];

    }
}