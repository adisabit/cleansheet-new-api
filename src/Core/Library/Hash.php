<?php

namespace Skeleton\Core\Library;

class Hash
{
    public static function sha256($data, $key)
    {
        return hash_hmac('sha256', $data, $key);
    }

    public static function sha512($data, $key)
    {
        return hash_hmac('sha512', $data, $key);
    }

    public static function md5(string $data)
    {
        return md5($data);
    }
}