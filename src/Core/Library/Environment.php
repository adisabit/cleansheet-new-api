<?php

namespace Skeleton\Core\Library;

abstract class Environment
{

    /**
     * Undocumented function
     *
     * @param string $key
     * @param $defaultValue
     * @return string
     */
    public static function get(string $varName, $defaultValue)
    {
        $value = getenv($varName);

        if (!$value) {
            return $defaultValue;
        }

        return $value;
    }
}