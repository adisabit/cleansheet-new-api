<?php

namespace Skeleton\Core;

class Api
{
    protected $service;
    protected $databaseName;

    /**
     * Set service
     *
     * @param $service
     * @return void
     */
    public function setService($service): void
    {
        $this->service = $service;
    }
}