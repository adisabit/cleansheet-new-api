<?php

namespace Skeleton\Core\System\Log;

use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    public function __construct($databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = new Model($this->databaseName);
    }

    /**
     * Insert log
     *
     * @param array $data
     * @return array
     */
    public function insert(array $data): array
    {
        $result = $this->model->insert($data);

        return $result;
    }
}