<?php

namespace Skeleton\Core\System\Log;

use Skeleton\Core\Database\Model as DatabaseModel;

class Model extends DatabaseModel
{
    /**
     * Log model constructor
     *
     * @param $databaseName
     */
    public function __construct($databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'api_log';
    }
}