<?php

namespace Skeleton\Core\Database;

use PDO;
use PDOException;
use Skeleton\Core\Library\Database;
use Skeleton\Core\Exception\DataNotFoundException;

class Model
{
    private $database;

    public $tableName;
    public $excludes = [];
    public $includes = [];

    /**
     * Model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName, PDO $pdo = NULL)
    {
        $this->database = (new Database($databaseName, $pdo))->getConnection();
    }

    /**
     * Filter query result
     *
     * @param array $data
     * @return void
     */
    public function filter(array $data)
    {
        if ( empty($this->includes) && !empty($this->excludes) ) {
            foreach ($this->excludes as $field) {
                unset($data[$field]);
            }
            
            $result = $data;
        } else if ( !empty($this->includes) && empty($this->excludes) ) {
            $result = [];

            foreach ($this->includes as $field) {
                $result[$field] = $data[$field];
            }
        } else if ( empty ($this->includes) && empty($this->excludes) ) {
            $result = $data;
        } else {
            $result = [];

            foreach ($this->includes as $field) {
                $result[$field] = $data[$field];
            }

            foreach ($this->excludes as $field) {
                unset($result[$field]);
            }
        }

        return $result;
    }

    /**
     * Get
     *
     * @param array $conditions
     * @param array $fields
     * @return array
     */
    public function fetch(string $query, array $conditions): array
    {
        $statement = $this->database->prepare($query);
        
        foreach ($conditions as $k => $v) {
            $statement->bindValue($k, $v, is_integer($v) ? PDO::PARAM_INT : PDO::PARAM_STR);
        }
        
        $success = $statement->execute();

        if (!$success) {
            $statementError = $statement->errorInfo();
            throw new PDOException($statementError[2], 500);
        }
        
        $fetchResults = $statement->fetchAll(PDO::FETCH_ASSOC);

        $result = [];

        foreach ($fetchResults as $fetchResult) {
            $result[] = $this->filter($fetchResult);

        }

        return $result;
    }

    /**
     * Insert data
     *
     * @param array $data
     * @return array
     */
    public function insert(array $data): array
    {
        $data['created_time'] = date("Y-m-d H:i:s");

        $fields = [];
        $params = [];
        $values = [];
        foreach ($data as $k => $v) {
            $params[] = ":$k";
            $values[":$k"] = $v;
            $fields[] = "`$k`";
        }

        $query = "INSERT INTO {$this->tableName} (" . implode(", ", $fields) . ") VALUES (" . implode(", ", $params) . ");";

        $statement = $this->database->prepare($query);

        foreach ($values as $k => $v) {
            $statement->bindValue($k, $v);
        }

        $success = $statement->execute();

        if (!$success) {
            $statementError = $statement->errorInfo();
            throw new PDOException($statementError[2], 500);
        }

        $id = $this->database->lastInsertId();

        return $this->fetch("SELECT * FROM {$this->tableName} WHERE id = :id", [
            ':id' => $id
        ])[0];
    }

    /**
     * Generate array key
     *
     * @param string $arrayKey
     * @return void
     */
    private function generateArrayKey(string $arrayKey)
    {
        return substr(md5(microtime()), 0, 8) . "_$arrayKey";
    }

    /**
     * Update
     *
     * @param array $data
     * @param array $conditions
     * @return array
     */
    public function update(array $data, array $conditions): array
    {
        $data['updated_time'] = date("Y-m-d H:i:s");

        $fields = [];
        $params = [];
        $values = [];
        foreach ($data as $k => $v) {
            $arrayKey = $this->generateArrayKey($k);

            $params[] = ":$arrayKey";
            $values[":$arrayKey"] = $v;
            $fields[] = "`$k` = :$arrayKey";
        }

        $conditionFields = [];
        foreach ($conditions as $k => $v) {
            $arrayKey = $this->generateArrayKey($k);

            $conditionFields[] = "$k = :$arrayKey";
            $values[":$arrayKey"] = $v;
        }

        $query = "UPDATE {$this->tableName} SET " . implode(", ", $fields) . " WHERE " . implode(" AND ", $conditionFields) . ";";

        $statement = $this->database->prepare($query);

        foreach ($values as $k => $v) {
            $statement->bindValue($k, $v);
        }

        $success = $statement->execute();
        if (!$success) {
            $statementError = $statement->errorInfo();
            throw new PDOException($statementError[2], 500);
        }

        return [
            'result'        => $statement->rowCount(),
            'updated_time'  => $data['updated_time']
        ];
    }

    /**
     * Delete record
     *
     * @param array $conditions
     * @return void
     */
    public function delete(array $conditions)
    {
        $deletedTime = date("Y-m-d H:i:s");

        $result = $this->update([
            'deleted_time' => $deletedTime
        ], $conditions);

        return [
            'result'        => $result['result'],
            'deleted_time'  => $deletedTime
        ];
    }

    /**
     * Get data
     *
     * @param array $conditions
     * @return void
     */
    public function find(array $conditions = NULL)
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL";

        $conditionFields = [];
        $conditionValues = [];

        if ($conditions != NULL) {
            foreach ($conditions as $k => $v) {
                $conditionFields[] = "$k = :$k";
                $conditionValues[":$k"] = $v; 
            }

            $query .= " AND " . implode(' AND ', $conditionFields);
        }

        $result = $this->fetch($query, $conditionValues);

        return $result;
    }
}