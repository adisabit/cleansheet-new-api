<?php

namespace Skeleton\Core;

use Skeleton\Core\Database\Model;

class HistoryModel extends Model
{
    const TYPE_INSERT = 'INSERT';
    const TYPE_UPDATE = 'UPDATE';
    const TYPE_DELETE = 'DELETE';

    /**
     * Core history model constructor
     *
     * @param $databaseName
     */
    public function __construct($databaseName)
    {
        parent::__construct($databaseName);
    }
}