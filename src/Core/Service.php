<?php

namespace Skeleton\Core;

use ErrorException;
use Skeleton\Core\Exception\DataNotFoundException;

class Service
{
    protected $model;
    protected $historyModel;
    protected $databaseName;

    /**
     * Set model
     *
     * @param $model
     * @return void
     */
    public function setModel($model): void
    {
        $this->model = $model;
    }

    /**
     * Set history model
     *
     * @param $historyModel
     * @return void
     */
    public function setHistoryModel($historyModel): void
    {
        $this->historyModel = $historyModel;
    }

    /**
     * Generate history data
     *
     * @param array $data
     * @param string $prefix
     * @return void
     */
    private function generateHistoryData(array $data, string $prefix, array $fields = [])
    {
        $result = [];

        foreach ($fields as $index) {
            try {
                $result[$prefix . '_' . $index] = $data[$index];
            } catch (ErrorException $e) {
                continue;
            }
        }

        return $result;
    }

    /**
     * Create new data
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        $item = $this->model->insert($data);
        $newData = $this->generateHistoryData($item, 'new', array_keys($data));

        return [
            'result'    => $item,
            'new_data'  => $newData
        ];
    }

    /**
     * Update data
     *
     * @param array $data
     * @param array $conditions
     * @return array
     */
    public function update(array $data, array $conditions): array
    {
        $item = $this->getOrFail($conditions);

        $result = $this->model->update($data, $conditions);

        $oldData = $this->generateHistoryData($item, 'old', array_keys($data));
        $newData = $this->generateHistoryData($data, 'new', array_keys($data));
        
        return [
            'result'    => $result['result'],
            'old_data'  => $oldData,
            'new_data'  => $newData 
        ];
    }

    /**
     * Get data or fail
     *
     * @param array $conditions
     * @return array
     */
    public function getOrFail(array $conditions): array
    {
        $item = $this->model->find($conditions);
        
        if ( count($item) == 0) {
            throw new DataNotFoundException('Data tidak ditemukan!', 204);
        }
        
        return $item[0];
    }

    /**
     * Delete data
     *
     * @param array $conditions
     * @return array
     */
    public function delete(array $conditions): array
    {
        $item = $this->getOrFail($conditions);
        
        $result = $this->model->delete($conditions);

        $oldData = $this->generateHistoryData($item, 'old', []);
        $newData = $this->generateHistoryData($item, 'new', []);
        
        return [
            'result'    => $result['result'],
            'old_data'  => $oldData,
            'new_data'  => $newData
        ];
    }

    /**
     * Get data
     *
     * @param int $id
     * @return array
     */
    public function getById(int $id): array
    {
        $data = $this->model->getById($id);

        return $data;
    }

    /**
     * Find all data
     *
     * @param array $conditions
     * @return array
     */
    public function find(array $conditions = NULL): array
    {
        $data = $this->model->find($conditions);

        return $data;
    }
}