<?php

namespace Skeleton\Core;

use Skeleton\Core\Database\Model as DatabaseModel;
use Skeleton\Core\Exception\DataNotFoundException;

class Model extends DatabaseModel
{
    /**
     * Core model constructor
     *
     * @param $databaseName
     */
    public function __construct($databaseName)
    {
        parent::__construct($databaseName);
    }

    /**
     * Get by id
     *
     * @param integer $id
     * @return array
     */
    public function getById(int $id): array
    {
        $data = $this->find(['id' => $id]);

        if (count($data) == 0) {
            throw new DataNotFoundException("Data tidak ditemukan!");
        }

        return $data[0];
    }
}