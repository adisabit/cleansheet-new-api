<?php

namespace Skeleton\Core\Helper;

abstract class Config
{
    public static $whiteList = [
        'image' => ['image/png', 'image/jpg', 'image/jpeg']
    ];
}