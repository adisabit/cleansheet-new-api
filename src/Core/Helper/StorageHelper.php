<?php

namespace Skeleton\Core\Helper;

use Skeleton\Core\Exception\HttpException;

abstract class StorageHelper
{
    const WHITELIST_IMAGE = 'image';

    /**
     * Generate file name
     *
     * @param string $base64Data
     * @param string $type
     * @return string
     */
    public static function generateFileName(string $base64Data, string $type): string
    {
        $base64DecodedData = base64_decode($base64Data);

        $f = finfo_open();
        $mimeType = finfo_buffer($f, $base64DecodedData, FILEINFO_MIME_TYPE);
        
        if (!in_array($mimeType, Config::$whiteList[$type])) {
            throw new HttpException("Mime type tidak diizinkan!", 400);
        }

        $extension = explode('/', $mimeType)[1];
        $fileName = md5(microtime()) . '.' . $extension;

        return $fileName;
    }
}