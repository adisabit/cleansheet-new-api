<?php

namespace Skeleton\Core\Exception;

use Exception;
use Throwable;

class HttpException extends Exception
{
    /**
     * HttpException constructor
     *
     * @param string $message
     * @param integer $code
     * @param Throwable $previous
     */
    public function __construct(string $message, int $code = 500, Throwable $previous = NULL)
    {
        parent::__construct($message, $code, $previous);
    }
}