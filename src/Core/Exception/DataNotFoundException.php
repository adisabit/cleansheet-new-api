<?php

namespace Skeleton\Core\Exception;

use Throwable;

class DataNotFoundException extends HttpException
{
    /**
     * DataNotFoundException constructor
     *
     * @param string $message
     * @param integer $code
     * @param Throwable $previous
     */
    public function __construct(string $message, int $code = 204, Throwable $previous = NULL)
    {
        parent::__construct($message, $code, $previous);
    }
}