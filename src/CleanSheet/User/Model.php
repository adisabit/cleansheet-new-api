<?php

namespace Cleansheet\User;

use Cleansheet\User\Config;
use Skeleton\Core\Model as CoreModel;
use Skeleton\Core\Exception\DataNotFoundException;

class Model extends CoreModel
{
    /**
     * User model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'user';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }

    /**
     * Check whether email is exist in table
     *
     * @param string $email
     * @return boolean
     */
    public function isEmailExist(string $email)
    {
        $email = $this->fetch("select email from {$this->tableName} where deleted_time is null and email = :email", [
            ':email' => $email
        ]);

        return count($email) > 0;
    }

    /**
     * Get user by email
     *
     * @param string $email
     * @return void
     */
    public function getByEmail(string $email)
    {
        $user = $this->find([ 'email' => $email ]);

        if (count($user) == 0) {
            throw new DataNotFoundException("User tidak ditemukan!", 204);
        }

        return $user[0];
    }

    /**
     * Check whether user exist
     *
     * @param integer $limit
     * @param integer $offset
     * @return boolean
     */
    public function areUserExists(int $limit, int $offset): bool
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL LIMIT :limit OFFSET :offset";

        $users = $this->fetch($query, [
            ':limit'    => $limit,
            ':offset'   => $offset
        ]);

        return count($users) > 0;
    }

    /**
     * Find users
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function findUsers(int $limit, int $offset): array
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL LIMIT :limit OFFSET :offset";

        $users = $this->fetch($query, [
            ':limit'    => $limit,
            ':offset'   => $offset
        ]);

        return $users;
    }
}