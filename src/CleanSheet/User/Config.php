<?php

namespace Cleansheet\User;

class Config
{
    public static $databaseName = "user";

    public static $directory = 'profile';

    public static $perPage = 8;
}