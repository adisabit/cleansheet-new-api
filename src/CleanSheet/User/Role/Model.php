<?php

namespace Cleansheet\User\Role;

use Cleansheet\Registrant\Config;
use Skeleton\Core\Model as CoreModel;

class Model extends CoreModel
{   
    /**
     * User role model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'user_role';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }
}