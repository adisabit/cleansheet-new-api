<?php

namespace Cleansheet\User\Role;

use Cleansheet\Master\Role\Service as RoleService;
use Cleansheet\User\Config;
use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    private $role;

    /**
     * User role service constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = new Model($this->databaseName);
        $this->historyModel = new HistoryModel($this->databaseName);

        $this->role = new RoleService();
    }

    /**
     * Insert new user role data
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        $userRole = parent::create($data);

        $historyData = array_merge($userRole['new_data'], [
            'user_role_id'  => $userRole['result']['id'],
            'type'          => HistoryModel::TYPE_INSERT
        ]);

        $this->historyModel->insert($historyData);

        return $userRole['result'];
    }

    /**
     * Find role by its user id
     *
     * @param integer $userId
     * @return array
     */
    public function findByUser(int $userId): array
    {
        $roles = $this->model->find([
            'user_id' => $userId
        ]);

        $result = [];

        foreach ($roles as $role) {
            $result[] = $this->role->getById($role['role_id']);
        }

        return $result;
    }

    /**
     * Check if user has a role
     *
     * @param integer $userId
     * @param integer $roleId
     * @return boolean
     */
    public function hasRole(int $userId, int $roleId): bool
    {
        $role = $this->model->find([
            'user_id'   => $userId,
            'role_id'   => $roleId
        ]);

        return count($role) > 0;
    }
}