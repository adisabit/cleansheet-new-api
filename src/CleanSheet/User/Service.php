<?php

namespace Cleansheet\User;

use Skeleton\Core\Service as CoreService;
use Skeleton\Core\Exception\HttpException;
use Skeleton\Core\Library\Request;

class Service extends CoreService
{
    /**
     * User service constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = new Model($this->databaseName);
        $this->historyModel = new HistoryModel($this->databaseName);
    }

    /**
     * Create new user
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        if ($this->model->isEmailExist($data['email'])) {
            throw new HttpException('Email telah digunakan!', 400);
        }

        $data['image_url'] = $data['image_url'] ?? 'https://gravatar.com/avatar/' . md5($data['email']) . '/?size=200&d=identicon';

        $result = parent::create($data);

        $this->historyModel->insert(array_merge($result['new_data'], [
            'user_id'   => $result['result']['id'],
            'type'      => HistoryModel::TYPE_INSERT 
        ]));

        return $result['result'];
    }

    /**
     * Get user by email
     *
     * @param string $email
     * @return array
     */
    public function getByEmail(string $email): array
    {
        $user = $this->model->getByEmail($email);

        return $user;
    }

    /**
     * Update profile data
     *
     * @param array $data
     * @param array $condition
     * @return array
     */
    public function update(array $data, array $condition): array
    {
        $user = $this->getById($condition['id']);

        if ($user['email'] != $data['email'] && $this->model->isEmailExist($data['email'])) {
            throw new HttpException('Email telah digunakan!', 400);
        }

        return parent::update($data, $condition);
    }

    /**
     * Get start and end
     *
     * @param integer $page
     * @param integer $itemPerPage
     * @return void
     */
    private function getLimitOffset(int $page, int $itemPerPage = NULL)
    {
        $itemPerPage = $itemPerPage ?? Config::$perPage;
        $offset = ($page - 1) * $itemPerPage;

        return [
            'limit'    => $itemPerPage,
            'offset'   => $offset
        ];
    }
    
    /**
     * Find Partner
     *
     * @param integer $page
     * @param integer $itemPerPage
     * @return array
     */
    public function list(int $page, int $itemPerPage = NULL): array
    {
        $current = $this->getLimitOffset($page, $itemPerPage);
        $prev = $this->getLimitOffset($page - 1, $itemPerPage);
        $next = $this->getLimitOffset($page + 1, $itemPerPage);

        $users = $current['offset'] < 0 ? [] : $this->model->findUsers($current['limit'], $current['offset']);

        $prevUserExists = $prev['offset'] < 0 ? false : $this->model->areUserExists($prev['limit'], $prev['offset']);
        $nextUserExists = $next['offset'] < 0 ? false : $this->model->areUserExists($next['limit'], $next['offset']);

        $prevPageUrl = NULL;
        if ($prevUserExists) {
            $prevPageUrl = rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?page=' . ($page - 1);
            $prevPageUrl .= $itemPerPage != NULL ? '&item_per_page=' . $itemPerPage : '';
        }

        $nextPageUrl = NULL;
        if ($nextUserExists) {
            $nextPageUrl = rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?page=' . ($page + 1);
            $nextPageUrl .= $itemPerPage != NULL ? '&item_per_page=' . $itemPerPage : '';
        }
        
        return [
            'users'         => $users,
            'prev_page_url' => $prevPageUrl,
            'next_page_url' => $nextPageUrl
        ];
    }
}