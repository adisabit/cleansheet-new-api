<?php

namespace Cleansheet\User\Auth;

use Cleansheet\User\Config;
use Skeleton\Core\Model as CoreModel;
use Skeleton\Core\Exception\DataNotFoundException;

class Model extends CoreModel
{
    /**
     * User auth model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'user_auth';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }

    /**
     * Get user auth by user id
     *
     * @param integer $userId
     * @return array
     */
    public function getByUser(int $userId): array
    {
        $userAuth = $this->find([ 'user_id' => $userId ]);

        if (count($userAuth) == 0) {
            throw new DataNotFoundException('User auth tidak ditemukan!', 204);
        }

        return $userAuth[0];
    }
}