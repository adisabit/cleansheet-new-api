<?php

namespace Cleansheet\User\Auth;

use Cleansheet\User\Config;
use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    /**
     * User auth service constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = new Model($this->databaseName);
        $this->historyModel = new HistoryModel($this->databaseName);
    }

    /**
     * Create user auth
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        $result = parent::create($data);

        unset($result['new_data']['new_user_id']);

        $this->historyModel->insert(array_merge($result['new_data'], [
            'user_auth_id'  => $result['result']['id'],
            'type'          => HistoryModel::TYPE_INSERT 
        ]));

        return $result['result'];
    }

    /**
     * Get user auth by user id
     *
     * @param integer $userId
     * @return array
     */
    public function getByUser(int $userId): array
    {
        $userAuth = $this->model->getByUser($userId);

        return $userAuth;
    }
}