<?php

namespace Cleansheet\User;

use Cleansheet\User\Config;
use Skeleton\Core\HistoryModel as CoreHistoryModel;

class HistoryModel extends CoreHistoryModel
{
    /**
     * User history model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'user_history';
    }
}