<?php

namespace Cleansheet\User;

use Cleansheet\Master\Role\Model as MasterModel;
use Exception;
use Cleansheet\User\Config;
use Skeleton\Core\Library\Hash;
use Skeleton\Core\Library\Token;
use Skeleton\Core\Api as CoreApi;
use Skeleton\Core\Library\Database;
use Skeleton\Core\Exception\HttpException;
use Cleansheet\User\Auth\Service as AuthService;
use Cleansheet\User\Role\Service as RoleService;
use Skeleton\Core\Exception\DataNotFoundException;
use Skeleton\Core\Helper\StorageHelper;
use Skeleton\Core\Library\Storage;

class Api extends CoreApi
{
    private $user;
    private $userAuth;
    private $userRole;

    private $claims = ['id', 'name', 'email', 'city'];
    
    /**
     * User api constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->user = new Service($this->databaseName);
        $this->userAuth = new AuthService($this->databaseName);
        $this->userRole = new RoleService($this->databaseName);
    }

    /**
     * Create new user
     *
     * @param string $name
     * @param string $email
     * @param string $city
     * @param string $password
     * @return array
     */
    public function register(array $data): array
    {
        $db = new Database($this->databaseName);
        $db->beginTransaction();

        try {
            $user = $this->user->create([
                'name'          => $data['name'],
                'email'         => $data['email'],
                'city'          => $data['city'] ?? NULL,
                'gender'        => $data['gender'] ?? NULL,
                'phone_number'  => $data['phone_number'] ?? NULL,
                'date_of_birth' => $data['date_of_birth'] ?? NULL,
                'image_url'     => $data['image_url'] ?? NULL
            ]);
            
            $salt = Hash::md5(microtime());
            $hashedPassword = Hash::sha512($data['password'], $salt);

            $this->userAuth->create([
                'user_id'   => $user['id'],
                'password'  => $hashedPassword,
                'salt'      => $salt,
            ]);

            $this->userRole->create([
                'user_id'   => $user['id'],
                'role_id'   => $data['role_id'] ?? MasterModel::ROLE_USER
            ]);
        
            $db->commit();
            
            return $user; 
        } catch (Exception $e) {
            $db->rollBack();
            
            throw $e instanceof HttpException ? $e : new HttpException('Tidak dapat melakukan registrasi!', 500, $e);
        }
    }

    /**
     * Log in a user
     *
     * @param string $email
     * @param string $password
     * @return array
     */
    public function login(string $email, string $password): array
    {
        try {
            $user = $this->user->getByEmail($email);
            $userAuth = $this->userAuth->getByUser($user['id']);

            $hashedPassword = Hash::sha512($password, $userAuth['salt']);

            if ($hashedPassword != $userAuth['password']) {
                throw new HttpException('Email / password tidak valid!', 401);
            }

            $user['is_admin'] = $this->userRole->hasRole($user['id'], MasterModel::ROLE_ADMIN);

            $token = Token::generate($user);

            $this->userAuth->update([
                'last_login_time'   => date('Y-m-d H:i:s'),
                'api_token'         => $token['access_token']
            ], [
                'user_id' => $user['id']
            ]);

            return array_merge($user, $token);
            
        } catch (Exception $e) {
            throw $e instanceof HttpException ? $e : new HttpException('Email / password tidak valid!', 401);
        }
    }

    /**
     * Refresh token
     *
     * @param string $accessToken
     * @param string $refreshToken
     * @return array
     */
    public function refreshToken(string $accessToken, string $refreshToken): array
    {
        $email = Token::getClaim($accessToken, 'email');
        
        $user = $this->user->getByEmail($email);

        $result = Token::refresh($accessToken, $refreshToken, $this->claims);

        $this->userAuth->update([
            'api_token'         => $result['access_token']
        ], [
            'user_id' => $user['id']
        ]);

        return array_merge($user, $result);
    }

    /**
     * Get by id
     *
     * @param integer $id
     * @return array
     */
    public function getById(int $id): array
    {
        try {
            $user = $this->user->getById($id);

            return $user;
        } catch (DataNotFoundException $e) {
            throw new DataNotFoundException('User tidak ditemukan!', $e->getCode());
        }
    }

    /**
     * Upload profile image
     *
     * @param string $profileImageBase64
     * @return array
     */
    public function uploadProfileImage(string $profileImageBase64): array
    {
        $fileName = StorageHelper::generateFileName($profileImageBase64, StorageHelper::WHITELIST_IMAGE);

        $storage = new Storage(\Cleansheet\Config::$bucketName[\CleanSheet\Config::$environment]);
    
        $result = $storage->uploadBase64($profileImageBase64, $fileName, Config::$directory);

        return $result;
    }

    /**
     * Update profile data
     *
     * @param integer $userId
     * @param array $data
     * @return integer
     */
    public function update(int $userId, array $data): int
    {
        $db = new Database($this->databaseName);
        $db->beginTransaction();

        try {
            $result = $this->user->update([
                'name'          => $data['name'],
                'email'         => $data['email'],
                'city'          => $data['city'],
                'gender'        => $data['gender'],
                'phone_number'  => $data['phone_number'],
                'date_of_birth' => $data['date_of_birth'],
                'image_url'     => $data['image_url']
            ], [
                'id' => $userId
            ]);
    
            if (isset($data['role_id'])) {
                $this->userRole->update([
                    'role_id'   => $data['role_id']
                ], [
                    'user_id'   => $userId
                ]);
            }
    
            if (!empty($data['password'])) {
                $salt = Hash::md5(microtime());
                $hashedPassword = Hash::sha512($data['password'], $salt);
    
                $this->userAuth->update([
                    'salt'      => $salt,
                    'password'  => $hashedPassword
                ], [
                    'user_id'   => $userId
                ]);
            }

            $db->commit();
    
            return $result['result'];
        } catch (Exception $e) {
            $db->rollBack();

            throw $e instanceof HttpException ? $e : new HttpException('Tidak dapat mengubah profile!', 500, $e);
        }
    }

    /**
     * Get list of users
     *
     * @param integer $page
     * @param integer $itemPerPage
     * @return array
     */
    public function list(int $page, int $itemPerPage = NULL): array
    {
        return $this->user->list($page, $itemPerPage);
    }

    /**
     * Delete usres
     *
     * @param integer $id
     * @return integer
     */
    public function delete(int $id): int
    {
        $db = new Database($this->databaseName);
        $db->beginTransaction();

        try {
            $result = $this->user->delete(['id' => $id]);
            $this->userAuth->delete(['user_id' => $id ]);
            $this->userRole->delete(['user_id' => $id ]);
    
            $db->commit();

            return $result['result'];
        } catch (Exception $e) {
            $db->rollBack();
            
            throw $e instanceof HttpException ? $e : new HttpException('Tidak dapat menghapus user!', 500, $e);
        }
    }
}