<?php

namespace Cleansheet\Partner;

use Skeleton\Core\Library\Request;
use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    /**
     * Partner service constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = new Model($this->databaseName);
        $this->historyModel = new HistoryModel($this->databaseName);
    }

    /**
     * Create new partner
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        $result = parent::create($data);

        $historyData = array_merge($result['new_data'], [
            'partner_id'    => $result['result']['id'],
            'type'          => HistoryModel::TYPE_INSERT
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }

    /**
     * Update Partner
     *
     * @param array $data
     * @return array
     */
    public function update(array $data, array $conditions): array
    {
        $item = $this->getOrFail($conditions);

        $result = parent::update($data, $conditions);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'partner_id'    => $item['id'],
            'type'          => HistoryModel::TYPE_UPDATE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }

    /**
     * Delete Partner
     *
     * @param array $data
     * @return array
     */
    public function delete(array $conditions): array
    {
        $item = $this->getOrFail($conditions);

        $result = parent::delete($conditions);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'partner_id'    => $item['id'],
            'type'          => HistoryModel::TYPE_DELETE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }

    /**
     * Get start and end
     *
     * @param integer $page
     * @param integer $itemPerPage
     * @return void
     */
    private function getLimitOffset(int $page, int $itemPerPage = NULL)
    {
        $itemPerPage = $itemPerPage ?? Config::$partnerPerPage;
        $offset = ($page - 1) * $itemPerPage;

        return [
            'limit'    => $itemPerPage,
            'offset'   => $offset
        ];
    }
    
    /**
     * Find Partner
     *
     * @param integer $page
     * @param integer $itemPerPage
     * @return array
     */
    public function findPartners(int $page, int $itemPerPage = NULL): array
    {
        $current = $this->getLimitOffset($page, $itemPerPage);
        $prev = $this->getLimitOffset($page - 1, $itemPerPage);
        $next = $this->getLimitOffset($page + 1, $itemPerPage);

        $currentPartners = $current['offset'] < 0 ? [] : $this->model->findPartners($current['limit'], $current['offset']);

        $prevPartnerExists = $prev['offset'] < 0 ? false : $this->model->arePartnerExists($prev['limit'], $prev['offset']);
        $nextPartnerExists = $next['offset'] < 0 ? false : $this->model->arePartnerExists($next['limit'], $next['offset']);

        $prevPageUrl = NULL;
        if ($prevPartnerExists) {
            $prevPageUrl = rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?page=' . ($page - 1);
            $prevPageUrl .= $itemPerPage != NULL ? '&item_per_page=' . $itemPerPage : '';
        }

        $nextPageUrl = NULL;
        if ($nextPartnerExists) {
            $nextPageUrl = rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?page=' . ($page + 1);
            $nextPageUrl .= $itemPerPage != NULL ? '&item_per_page=' . $itemPerPage : '';
        }
        
        return [
            'partners'      => $currentPartners,
            'prev_page_url' => $prevPageUrl,
            'next_page_url' => $nextPageUrl
        ];
    }
}