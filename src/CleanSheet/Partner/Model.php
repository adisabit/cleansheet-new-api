<?php

namespace Cleansheet\Partner;

use Skeleton\Core\Model as CoreModel;

class Model extends CoreModel
{
    /**
     * Partner model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'partner';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }

    /**
     * Check whether partner exist
     *
     * @param integer $limit
     * @param integer $offset
     * @return boolean
     */
    public function arePartnerExists(int $limit, int $offset): bool
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL LIMIT :limit OFFSET :offset";

        $partners = $this->fetch($query, [
            ':limit'    => $limit,
            ':offset'   => $offset
        ]);

        return count($partners) > 0;
    }

    /**
     * Find partners
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function findPartners(int $limit, int $offset): array
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL LIMIT :limit OFFSET :offset";

        $partners = $this->fetch($query, [
            ':limit'    => $limit,
            ':offset'   => $offset
        ]);

        return $partners;
    }
}