<?php

namespace Cleansheet\Partner;

abstract class Config
{
    public static $databaseName = 'partner';

    public static $partnerPerPage = 10;
}