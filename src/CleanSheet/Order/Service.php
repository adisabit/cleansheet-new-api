<?php

namespace Cleansheet\Order;

use Skeleton\Core\Library\Hash;
use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    /**
     * Generate order ref
     *
     * @return string
     */
    public function generateOrderRef(string $moduleName): string
    {
        $hash = Hash::md5(microtime());

        $orderRef = sprintf(
            "%s/%s/%s",
            Config::$orderRefPrefixes['name'],
            Config::$orderRefPrefixes['module'][$moduleName],
            substr(strtoupper($hash), Config::$substringStartIndex, Config::$substringLength)
        );

        return $orderRef;
    }
}