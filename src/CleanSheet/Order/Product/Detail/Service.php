<?php

namespace Cleansheet\Order\Product\Detail;

use Cleansheet\Order\Config;
use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    /**
     * Product detail service constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = new Model($this->databaseName);
        $this->historyModel = new HistoryModel($this->databaseName);
    }

    /**
     * Insert order product detail data
     *
     * @param int $orderProductId
     * @param array $data
     * @return array
     */
    public function insert(int $orderProductId, array $data): array
    {
        $result = parent::create(array_merge($data, [
            'order_product_id'  => $orderProductId
        ]));

        return $result['result'];
    }

    /**
     * Get by order product
     *
     * @param integer $orderProductId
     * @return array
     */
    public function findByOrderProduct(int $orderProductId): array
    {
        $details = $this->model->find([
            'order_product_id' => $orderProductId
        ]);

        return $details;
    }
}