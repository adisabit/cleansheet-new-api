<?php

namespace Cleansheet\Order\Product\Detail;

use Cleansheet\Order\Config;
use Skeleton\Core\Model as CoreModel;

class Model extends CoreModel
{
    /**
     * Product cart model
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'order_product_detail';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }
}