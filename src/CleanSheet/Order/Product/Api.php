<?php

namespace Cleansheet\Order\Product;

use Cleansheet\Order\Config;
use Cleansheet\Order\Product\Detail\Service as DetailService;
use Skeleton\Core\Api as CoreApi;
use Skeleton\Core\Exception\HttpException;

class Api extends CoreApi
{
    private $detail;

    /**
     * Order product api constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->service = new Service($databaseName);
        $this->detail = new DetailService($this->databaseName);
    }

    /**
     * Find new orders by user id
     *
     * @param integer $userId
     * @return array
     */
    public function findByUser(int $userId): array
    {
        $orderProducts = $this->service->find([
            'user_id'   => $userId,
        ]);

        foreach ($orderProducts as $k => $orderProduct) {
            $orderProducts[$k]['detail'] = $this->detail->findByOrderProduct($orderProduct['id']);
        }

        return $orderProducts;
    }

    /**
     * Get all products
     *
     * @param integer $page
     * @param integer $itemPerPage
     * @return array
     */
    public function getAllProducts(int $page, int $itemPerPage = NULL): array
    {
        $orderProducts = $this->service->getAllProducts($page, $itemPerPage);

        foreach ($orderProducts['products'] as $k => $orderProduct) {
            $orderProducts['products'][$k]['order_products'] = $this->detail->findByOrderProduct($orderProduct['id']);
        }

        return $orderProducts;
    }

    /**
     * Get by id
     *
     * @param integer $id
     * @return array
     */
    public function getById(int $id): array
    {
        $orderProduct = $this->service->getById($id);
        $orderProduct['order_products'] = $this->detail->findByOrderProduct($orderProduct['id']);

        return $orderProduct;
    }

    /**
     * Pay order product
     *
     * @param integer $orderProductId
     * @return integer
     */
    public function pay(int $orderProductId, string $remark = NULL): int
    {
        $result = $this->service->pay($orderProductId, $remark);

        return $result['result'];
    }

    /**
     * Reject order product
     *
     * @param integer $orderProductId
     * @param string $remark
     * @return integer
     */
    public function reject(int $orderProductId, string $remark): int
    {
        if ( empty($remark) ) {
            throw new HttpException('Field remark harus diisi!', 400);
        }
        
        $result = $this->service->reject($orderProductId, $remark);

        return $result['result'];
    }

    /**
     * Process order product
     *
     * @param integer $orderProductId
     * @param string $remark
     * @return integer
     */
    public function process(int $orderProductId, string $remark = NULL): int
    {
        $result = $this->service->process($orderProductId, $remark);

        return $result['result'];
    }

    /**
     * Complete order product
     *
     * @param integer $orderProductId
     * @param string $remark
     * @return integer
     */
    public function complete(int $orderProductId, string $remark = NULL): int
    {
        $result = $this->service->complete($orderProductId, $remark);

        return $result['result'];
    }
}