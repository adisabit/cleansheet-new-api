<?php

namespace Cleansheet\Order\Product;

use Cleansheet\Order\Config;
use Cleansheet\Order\Model as OrderModel;

class Model extends OrderModel
{    
    /**
     * Order product model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'order_product';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }
    
    /**
     * Get all products
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function getAllProducts(int $limit = NULL, int $offset = NULL): array
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL  ORDER BY created_time DESC";

        if ( isset($limit) && isset($offset) ) {
            $query .= ' LIMIT :limit OFFSET :offset';
            $params = [
                ':limit'    => $limit,
                ':offset'   => $offset
            ];
        } else {
            $params = [];
        }

        $products = $this->fetch($query, $params);

        return $products;
    }

    /**
     * Check whether products at current range are exist
     *
     * @param integer $limit
     * @param integer $offset
     * @return bool
     */
    public function areProductsExist(int $limit, int $offset): bool
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL ORDER BY created_time DESC LIMIT :limit OFFSET :offset";

        $products = $this->fetch($query, [
            ':limit'    => $limit,
            ':offset'   => $offset
        ]);

        return count($products) > 0;
    }
}