<?php

namespace Cleansheet\Order\Product\Payment;

use Cleansheet\Order\Config;
use Cleansheet\Order\Product\Service as OrderProductService;
use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    /**
     * Product detail service constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = new Model($this->databaseName);
        $this->historyModel = new HistoryModel($this->databaseName);
    }

    /**
     * Check whether
     *
     * @param integer $orderProductId
     * @return boolean
     */
    public function isAlreadyPaid(int $orderProductId): bool
    {
        $isAlreadyPaid = $this->model->isAlreadyPaid($orderProductId);

        return $isAlreadyPaid;
    }

    /**
     * Insert new data
     *
     * @param array $data
     * @return array
     */
    public function insert(array $data): array
    {
        $result = parent::create(array_merge($data, [
            'expiry_time'   => date("Y-m-d H:i:s", strtotime("now + 4 hour"))
        ]));

        $this->historyModel->insert(array_merge($result['new_data'], [
            'order_product_payment_id'  => $result['result']['id'],
            'type'                      => HistoryModel::TYPE_INSERT
        ]));

        return $result['result'];
    }

    /**
     * Update order product payment data
     *
     * @param array $data
     * @param array $conditions
     * @return array
     */
    public function update(array $data, array $conditions): array
    {
        $payment = $this->model->getByOrderProduct($conditions['order_product_id']);

        $result = parent::update($data, $conditions);

        unset($result['old_data']['old_order_product_id']);
        unset($result['new_data']['new_order_product_id']);

        $this->historyModel->insert(array_merge($result['old_data'], $result['new_data'], [
            'order_product_payment_id'  => $payment['id'],
            'type'                      => HistoryModel::TYPE_UPDATE        
        ]));

        return $result;
    }

    /**
     * Get by order product
     *
     * @param integer $orderProductId
     * @return array
     */
    public function getByOrderProduct(int $orderProductId): array
    {
        $payment = $this->model->getByOrderProduct($orderProductId);

        return $payment;
    }
}