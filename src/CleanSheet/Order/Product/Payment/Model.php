<?php

namespace Cleansheet\Order\Product\Payment;

use Cleansheet\Order\Config;
use Skeleton\Core\Model as CoreModel;
use Skeleton\Core\Exception\DataNotFoundException;

class Model extends CoreModel
{
    /**
     * Product cart model
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'order_product_payment';
        $this->excludes = ['order_product_id', 'created_time', 'updated_time', 'deleted_time'];
    }

    /**
     * Check whether order product is already paid
     *
     * @param integer $orderProductId
     * @return boolean
     */
    public function isAlreadyPaid(int $orderProductId): bool
    { 
        $payment = $this->find([ 'order_product_id' => $orderProductId ]);

        return count($payment) > 0;
    }

    /**
     * Get by order product id
     *
     * @param integer $orderProductId
     * @return array
     */
    public function getByOrderProduct(int $orderProductId): array
    { 
        $payment = $this->find([ 'order_product_id' => $orderProductId ]);

        if (count($payment) == 0) {
            throw new DataNotFoundException("Pembayaran produk tidak ditemukan!");
        }

        return $payment[0];
    }
}