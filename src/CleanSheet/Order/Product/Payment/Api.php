<?php

namespace Cleansheet\Order\Product\Payment;

use Cleansheet\Order\Config;
use Cleansheet\Order\Product\Service as OrderProductService;
use Skeleton\Core\Api as CoreApi;

class Api extends CoreApi
{
    private $orderService;

    /**
     * Order product payment api constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->service = new Service($this->databaseName);
        $this->orderService = new OrderProductService($this->databaseName);
    }

    /**
     * Save payment data
     *
     * @param array $data
     * @return array
     */
    public function pay(array $data): array
    {
        $isAleadyPaid = $this->service->isAlreadyPaid($data['order_product_id']);

        if ($isAleadyPaid) {
            $this->service->update($data, [ 'order_product_id' => $data['order_product_id'] ]);

            $result = $this->service->getByOrderProduct($data['order_product_id']);
            
        } else {
            $result = $this->service->insert($data);
        }

        $order = $this->orderService->getById($data['order_product_id']);

        $result['amount'] = $order['total'];

        return $result;
    }
}