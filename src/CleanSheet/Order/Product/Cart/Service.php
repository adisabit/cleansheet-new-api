<?php

namespace Cleansheet\Order\Product\Cart;

use Cleansheet\Order\Config;
use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    /**
     * Product cart service
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = new Model($this->databaseName);
        $this->historyModel = new HistoryModel($this->databaseName);
    }

    /**
     * Check whether product is exist for specific user
     *
     * @param integer $productId
     * @param integer $userId
     * @return boolean
     */
    public function isProductExist(int $productId, int $userId): bool
    {
        $isExist = $this->model->isProductExist($productId, $userId);

        return $isExist;
    }

    /**
     * Insert product to cart
     *
     * @param integer $productId
     * @param integer $userId
     * @param integer $amount
     * @return array
     */
    public function insert(int $productId, int $userId, int $amount): array
    {
        $result = parent::create([
            'product_id'    => $productId,
            'user_id'       => $userId,
            'amount'        => $amount,
            'status'        => Model::STATUS_SHIP
        ]);
        
        unset($result['new_data']['new_user_id']);
        unset($result['new_data']['new_product_id']);

        $this->historyModel->insert(array_merge($result['new_data'], [
            'product_cart_id'   => $result['result']['id'],
            'type'      => HistoryModel::TYPE_INSERT
        ]));

        return $result['result'];
    }

    /**
     * Ship product to cart
     *
     * @param integer $productId
     * @param integer $userId
     * @param integer $amount
     * @return array
     */
    public function update(array $data, array $conditions): array
    {
        $product = $this->model->getProduct($conditions['product_id'], $conditions['user_id']);

        $amount = $product['amount'] + $data['amount'];
        $amount = $amount < 0 ? 1 : $amount;

        $result = parent::update(['amount' => $amount], array_merge($conditions, ['status' => Model::STATUS_SHIP]));

        $this->historyModel->insert(array_merge($result['old_data'], $result['new_data'], [
            'product_cart_id'   => $product['id'],
            'type'              => HistoryModel::TYPE_UPDATE
        ]));

        return [
            'result' => $result['result']
        ];
    }

    /**
     * Remove product from cart
     *
     * @param integer $productId
     * @param integer $userId
     * @return int
     */
    public function remove(int $productId, int $userId): int
    {
        $product = $this->model->getProduct($productId, $userId);

        $result = parent::update(['status' => Model::STATUS_REMOVE], [
            'product_id'    => $productId,
            'user_id'       => $userId,
            'status'        => Model::STATUS_SHIP
        ]);

        $this->historyModel->insert(array_merge($result['old_data'], $result['new_data'], [
            'product_cart_id'   => $product['id'],
            'type'              => HistoryModel::TYPE_UPDATE
        ]));

        return $result['result'];
    }

    /**
     * Checkout product from cart
     *
     * @param integer $productId
     * @param integer $userId
     * @return int
     */
    public function checkout(int $productId, int $userId): int
    {
        $product = $this->model->getProduct($productId, $userId);

        $result = parent::update(['status' => Model::STATUS_CHECKOUT], [
            'product_id'    => $productId,
            'user_id'       => $userId,
            'status'        => Model::STATUS_SHIP
        ]);

        $this->historyModel->insert(array_merge($result['old_data'], $result['new_data'], [
            'product_cart_id'   => $product['id'],
            'type'              => HistoryModel::TYPE_UPDATE
        ]));

        return $result['result'];
    }

    /**
     * Find cart data
     *
     * @param integer $userId
     * @return array
     */
    public function findCartData(int $userId): array
    {
        $productCarts = $this->model->find([
            'user_id'   => $userId,
            'status'    => Model::STATUS_SHIP,
        ]);

        return $productCarts;
    }

    /**
     * Get product
     *
     * @param integer $productId
     * @param integer $userId
     * @return array
     */
    public function getProduct(int $productId, int $userId): array
    {
        $product = $this->model->getProduct($productId, $userId);

        return $product;
    }
}