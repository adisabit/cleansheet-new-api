<?php

namespace Cleansheet\Order\Product\Cart;

use Exception;
use Cleansheet\Order\Config;
use Skeleton\Core\Api as CoreApi;
use Skeleton\Core\Library\Database;
use Skeleton\Core\Exception\HttpException;
use Cleansheet\Order\Product\Service as OrderProductService;
use Cleansheet\Master\Product\Service as MasterProductService;
use Cleansheet\Order\Product\Detail\Service as OrderProductDetailService;

class Api extends CoreApi
{
    private $masterProduct;
    private $orderProduct;
    private $orderProductDetail;

    /**
     * Product cart api constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->service = new Service($this->databaseName);
        $this->masterProduct = new MasterProductService();
        $this->orderProduct = new OrderProductService($this->databaseName);
        $this->orderProductDetail = new OrderProductDetailService($this->databaseName);
    }

    /**
     * Ship product to cart
     *
     * @param integer $productId
     * @param integer $userId
     * @param integer $amount
     * @return array
     */
    public function ship(int $productId, int $userId, int $amount): array
    {
        if ( !$this->service->isProductExist($productId, $userId) ) {
            $amount = $amount <= 0 ? 1 : $amount;

            $result = $this->service->insert($productId, $userId, $amount);
        } else {
            $this->service->update(['amount' => $amount], [
                'product_id'    => $productId,
                'user_id'       => $userId
            ]);

            $result = $this->service->getProduct($productId, $userId);
        }

        $result['product'] = $this->masterProduct->getById($productId);

        return $result;
    }

    /**
     * Remoove product from cart
     *
     * @param integer $productId
     * @param integer $userId
     * @return integer
     */
    public function remove(int $productId, int $userId): int
    {
        $result = $this->service->remove($productId, $userId);

        return $result;
    }

    /**
     * Checkout
     *
     * @param integer $userId
     * @param array $data
     * @return array
     */
    public function checkout(int $userId, array $data): array
    {
        $productCarts = $this->findCartData($userId);
        
        if (count($productCarts['products']) == 0) {
            throw new HttpException("Tidak ada produk dalam keranjang!", 400);
        }

        $db = new Database($this->databaseName);
        $db->beginTransaction();

        try {
            $orderProduct = $this->orderProduct->insert($userId, array_merge($data, [
                'total' => $productCarts['total']
            ]));


            foreach ($productCarts['products'] as $product) {
                $this->service->checkout($product['product_id'], $userId);
                $detail = [
                    'name'      => $product['product']['name'],
                    'price'     => $product['product']['price'],
                    'amount'    => $product['amount'],
                    'image_url' => $product['product']['image_url'],
                ];

                $this->orderProductDetail->insert($orderProduct['id'], $detail);
                
                $orderProduct['products'][] = $product;
            }

            $db->commit();

            return $orderProduct;
        } catch (Exception $e) {
            $db->rollBack();

            throw $e instanceof HttpException ? $e : new HttpException("Tidak dapat melakukan checkout!", 500, $e);
        }
    }

    /**
     * Find cart data
     *
     * @param integer $userId
     * @return array
     */
    public function findCartData(int $userId): array
    {
        $productCarts = $this->service->findCartData($userId);

        $total = 0;

        foreach ($productCarts as $k => $v) {
            $product = $this->masterProduct->getById($v['product_id']);
            
            $productCarts[$k]['product'] = $product;

            $total = $total + ($product['price'] * $v['amount']);
        }

        return [
            'total'     => $total,
            'products'  => $productCarts
        ];
    }
}
