<?php

namespace Cleansheet\Order\Product\Cart;

use Cleansheet\Order\Config;
use Skeleton\Core\Model as CoreModel;
use Skeleton\Core\Exception\DataNotFoundException;

class Model extends CoreModel
{
    const STATUS_SHIP = 'SHIP';
    const STATUS_REMOVE = 'REMOVE';
    const STATUS_CHECKOUT = 'CHECKOUT';

    /**
     * Product cart model
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'order_product_cart';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }

    /**
     * Get product
     *
     * @param integer $productId
     * @param integer $userId
     * @return array
     */
    public function getProduct(int $productId, int $userId): array
    {
        $product = $this->find([
            'product_id'    => $productId,
            'user_id'       => $userId,
            'status'        => self::STATUS_SHIP
        ]);

        if (count($product) == 0) {
            throw new DataNotFoundException("Produk tidak ditemukan!");
        }

        return $product[0];
    }

    /**
     * Check whether product is exist
     *
     * @param integer $productId
     * @param integer $userId
     * @return boolean
     */
    public function isProductExist(int $productId, int $userId): bool
    {
        $product = $this->find([
            'product_id'    => $productId,
            'user_id'       => $userId,
            'status'        => self::STATUS_SHIP
        ]);

        return count($product) > 0;
    }
}