<?php

namespace Cleansheet\Order\Product;

use Cleansheet\Order\Config;
use Skeleton\Core\Library\Request;
use Cleansheet\Order\Service as OrderService;

class Service extends OrderService
{
    private $moduleName = 'product';

    /**
     * Order product service constructor
     *
     * @param string $databaseName
     * @param $model
     * @param $historyModel
     */ 
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = new Model($this->databaseName);
        $this->historyModel = new HistoryModel($this->databaseName);
    }

    /**
     * Create new order
     *
     * @param integer $userId
     * @param array $data
     * @return array
     */
    public function insert(int $userId, array $data): array
    {
        $orderRef = $this->generateOrderRef($this->moduleName);

        $result = parent::create(array_merge($data, [
            'user_id'   => $userId,
            'order_ref' => $orderRef,
            'status'    => Model::STATUS_NEW
        ]));

        $this->historyModel->insert([
            'order_product_id'  => $result['result']['id'],
            'type'              => HistoryModel::TYPE_INSERT,
            'new_status'        => Model::STATUS_NEW
        ]);

        return $result['result'];
    }

    /**
     * Get limit offset
     *
     * @param integer $page
     * @param integer $itemPerPage
     * @return void
     */
    private function getLimitOffset(int $page, int $itemPerPage = NULL)
    {
        $itemPerPage = $itemPerPage ?? Config::$productPerPage;
        $offset = ($page - 1) * $itemPerPage;

        return [
            'limit'    => $itemPerPage,
            'offset'   => $offset
        ];
    }
    
    /**
     * Get all products
     *
     * @return array
     */
    public function getAllProducts(int $page, int $itemPerPage = NULL): array
    {
        $current = $this->getLimitOffset($page, $itemPerPage);
        $prev = $this->getLimitOffset($page - 1, $itemPerPage);
        $next = $this->getLimitOffset($page + 1, $itemPerPage);

        $currentProducts = $current['offset'] < 0 ? [] : $this->model->getAllProducts($current['limit'], $current['offset']);
        $prevProductsExist = $prev['offset'] < 0 ? false : $this->model->areProductsExist($prev['limit'], $prev['offset']);
        $nextProductsExist = $next['offset'] < 0 ? false : $this->model->areProductsExist($next['limit'], $next['offset']);

        $prevPageUrl = NULL;
        if ($prevProductsExist) {
            $prevPageUrl = rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?page=' . ($page - 1);
            $prevPageUrl .= $itemPerPage != NULL ? '&item_per_page=' . $itemPerPage : '';
        }

        $nextPageUrl = NULL;
        if ($nextProductsExist) {
            $nextPageUrl = rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?page=' . ($page + 1);
            $nextPageUrl .= $itemPerPage != NULL ? '&item_per_page=' . $itemPerPage : '';
        }

        $allProducts = $this->model->getAllProducts();

        return [
            'products'      => $currentProducts,
            'prev_page_url' => $prevPageUrl,
            'next_page_url' => $nextPageUrl,
            'total'         => count($allProducts),
        ];
    }

    /**
     * Pay order product
     *
     * @param integer $orderProductId
     * @param string $remark
     * @return void
     */
    public function pay(int $orderProductId, string $remark = NULL)
    {
        $result = parent::update([
            'status' => Model::STATUS_PAID,
            'remark' => $remark
        ], ['id' => $orderProductId]);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'order_product_id' => $orderProductId,
            'type' => HistoryModel::TYPE_UPDATE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }

    /**
     * Reject order product
     *
     * @param integer $orderProductId
     * @param string $remark
     * @return void
     */
    public function reject(int $orderProductId, string $remark)
    {
        $result = parent::update([
            'status' => Model::STATUS_REJECTED,
            'remark' => $remark
        ], ['id' => $orderProductId]);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'order_product_id' => $orderProductId,
            'type' => HistoryModel::TYPE_UPDATE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }

    /**
     * Process order product
     *
     * @param integer $orderProductId
     * @param string $remark
     * @return void
     */
    public function process(int $orderProductId, string $remark = NULL)
    {
        $result = parent::update([
            'status' => Model::STATUS_PROCESSED,
            'remark' => $remark
        ], ['id' => $orderProductId]);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'order_product_id' => $orderProductId,
            'type' => HistoryModel::TYPE_UPDATE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }

    /**
     * Complete order product
     *
     * @param integer $orderProductId
     * @param string $remark
     * @return void
     */
    public function complete(int $orderProductId, string $remark = NULL)
    {
        $result = parent::update([
            'status' => Model::STATUS_COMPLETED,
            'remark' => $remark
        ], ['id' => $orderProductId]);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'order_product_id' => $orderProductId,
            'type' => HistoryModel::TYPE_UPDATE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }
}