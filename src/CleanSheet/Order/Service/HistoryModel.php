<?php

namespace Cleansheet\Order\Service;

use Cleansheet\Order\Config;
use Skeleton\Core\HistoryModel as CoreHistoryModel;

class HistoryModel extends CoreHistoryModel
{    
    /**
     * Order service history model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'order_service_history';
    } 
}