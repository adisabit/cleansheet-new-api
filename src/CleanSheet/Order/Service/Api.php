<?php

namespace Cleansheet\Order\Service;

use Cleansheet\Master\Service\Service as MasterServiceService;
use Cleansheet\Order\Config;
use Skeleton\Core\Api as CoreApi;
use Skeleton\Core\Exception\HttpException;

class Api extends CoreApi
{
    private $masterService;

    /**
     * Order service api constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->service = new Service($this->databaseName);
        $this->masterService = new MasterServiceService();
    }

    /**
     * Create order service
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        /*
        $exist = $this->service->isOrderExist($data['user_id']);

        if ($exist) {
            throw new HttpException('Tidak dapat membuat order baru dikarenakan order sebelumnya belum selesai!', 400);
        }
        */

        $service = $this->masterService->getById($data['service_id']);
        $data['total_price'] = $service['price'];

        $orderService = $this->service->create($data);

        $orderService['detail'] = $service;

        return $orderService;
    }

    /**
     * Find by user
     *
     * @param integer $userId
     * @return arrayUndocumented function
     */
    public function findByUser(int $userId): array
    {
        $orderServices = $this->service->find([
            'user_id' => $userId
        ]);

        foreach ($orderServices as $k => $orderService) {
            $orderServices[$k]['detail'] = $this->masterService->getById($orderService['service_id']);
        }
 
        return $orderServices;
    }

    /**
     * Get all service orders
     *
     * @param integer $page
     * @param integer $itemPerPage
     * @return array
     */
    public function getAllServices(int $page, int $itemPerPage = NULL): array
    {
        $orderServices = $this->service->getAllServices($page, $itemPerPage);

        foreach ($orderServices['services'] as $k => $orderService) {
            $orderServices['services'][$k]['order_services'] = $this->masterService->getById($orderService['service_id']);
        }

        return $orderServices;
    }

    /**
     * Get service order by its id
     *
     * @param integer $id
     * @return void
     */
    public function getById(int $id)
    {
        $service = $this->service->getById($id);
        $service['order_services'] = $this->masterService->getById($service['service_id']);

        return $service;
    }

    /**
     * Pay order service
     *
     * @param integer $orderServiceId
     * @return integer
     */
    public function pay(int $orderServiceId, string $remark = NULL): int
    {
        $result = $this->service->pay($orderServiceId, $remark);

        return $result['result'];
    }

    /**
     * Reject order service
     *
     * @param integer $orderServiceId
     * @param string $remark
     * @return integer
     */
    public function reject(int $orderServiceId, string $remark): int
    {
        if ( empty($remark) ) {
            throw new HttpException('Field remark harus diisi!', 400);
        }
        
        $result = $this->service->reject($orderServiceId, $remark);

        return $result['result'];
    }

    /**
     * Process order service
     *
     * @param integer $orderServiceId
     * @param string $remark
     * @return integer
     */
    public function process(int $orderServiceId, string $remark = NULL): int
    {
        $result = $this->service->process($orderServiceId, $remark);

        return $result['result'];
    }

    /**
     * Complete order service
     *
     * @param integer $orderServiceId
     * @param string $remark
     * @return integer
     */
    public function complete(int $orderServiceId, string $remark = NULL): int
    {
        $result = $this->service->complete($orderServiceId, $remark);

        return $result['result'];
    }
}