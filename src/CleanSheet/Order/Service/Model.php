<?php

namespace Cleansheet\Order\Service;

use Cleansheet\Order\Config;
use Cleansheet\Order\Model as OrderModel;
use Skeleton\Core\Model as CoreModel;

class Model extends OrderModel
{    
    /**
     * Order service model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'order_service';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    } 

    /**
     * Check whether order is exist
     *
     * @param integer $userId
     * @return boolean
     */
    public function isOrderExist(int $userId): bool
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL AND user_id = :user_id AND status IN (:first_status, :second_status, :third_status)";

        $result = $this->fetch($query, [
            ':user_id'          => $userId,
            ':first_status'     => self::STATUS_NEW,
            ':second_status'    => self::STATUS_PAID,
            ':third_status'     => self::STATUS_PROCESSED
        ]);

        return count($result) > 0;
    }


    /**
     * Get all services
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function getAllServices(int $limit = NULL, int $offset = NULL): array
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL  ORDER BY created_time DESC";

        if ( isset($limit) && isset($offset) ) {
            $query .= ' LIMIT :limit OFFSET :offset';
            $params = [
                ':limit'    => $limit,
                ':offset'   => $offset
            ];
        } else {
            $params = [];
        }

        $services = $this->fetch($query, $params);

        return $services;
    }

    /**
     * Check whether services at current range are exist
     *
     * @param integer $limit
     * @param integer $offset
     * @return bool
     */
    public function areServicesExist(int $limit, int $offset): bool
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL ORDER BY created_time DESC LIMIT :limit OFFSET :offset";

        $services = $this->fetch($query, [
            ':limit'    => $limit,
            ':offset'   => $offset
        ]);

        return count($services) > 0;
    }
}