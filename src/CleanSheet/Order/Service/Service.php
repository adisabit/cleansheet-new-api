<?php

namespace Cleansheet\Order\Service;

use Cleansheet\Order\Config;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Service as CoreService;
use Cleansheet\Order\Service as OrderService;

class Service extends OrderService
{
    private $moduleName = 'service';

    /**
     * Order service constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = new Model($this->databaseName);
        $this->historyModel = new HistoryModel($this->databaseName);
    }

    /**
     * Create order service
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        $orderRef = $this->generateOrderRef($this->moduleName);

        $data = array_merge($data, [
            'status'    => Model::STATUS_NEW,
            'order_ref' => $orderRef 
        ]);

        $orderService = parent::create($data);

        $this->historyModel->insert([
            'order_service_id'  => $orderService['result']['id'],
            'type'              => HistoryModel::TYPE_INSERT,
            'new_status'        => Model::STATUS_NEW
        ]);

        return $orderService['result'];
    }

    /**
     * Check order is exist
     *
     * @param integer $userId
     * @return boolean
     */
    public function isOrderExist(int $userId): bool
    {
        $exist = $this->model->isOrderExist($userId);

        return $exist;
    }

    /**
     * Get limit offset
     *
     * @param integer $page
     * @param integer $itemPerPage
     * @return void
     */
    private function getLimitOffset(int $page, int $itemPerPage = NULL)
    {
        $itemPerPage = $itemPerPage ?? Config::$productPerPage;
        $offset = ($page - 1) * $itemPerPage;

        return [
            'limit'    => $itemPerPage,
            'offset'   => $offset
        ];
    }
    
    /**
     * Get all services
     * 
     * @param int $page
     * @param int $itemPerPage
     * @return array
     */
    public function getAllServices(int $page, int $itemPerPage = NULL): array
    {
        $current = $this->getLimitOffset($page, $itemPerPage);
        $prev = $this->getLimitOffset($page - 1, $itemPerPage);
        $next = $this->getLimitOffset($page + 1, $itemPerPage);

        $currentServices = $current['offset'] < 0 ? [] : $this->model->getAllServices($current['limit'], $current['offset']);
        $prevServicesExist = $prev['offset'] < 0 ? false : $this->model->areServicesExist($prev['limit'], $prev['offset']);
        $nextServicesExist = $next['offset'] < 0 ? false : $this->model->areServicesExist($next['limit'], $next['offset']);

        $prevPageUrl = NULL;
        if ($prevServicesExist) {
            $prevPageUrl = rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?page=' . ($page - 1);
            $prevPageUrl .= $itemPerPage != NULL ? '&item_per_page=' . $itemPerPage : '';
        }

        $nextPageUrl = NULL;
        if ($nextServicesExist) {
            $nextPageUrl = rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?page=' . ($page + 1);
            $nextPageUrl .= $itemPerPage != NULL ? '&item_per_page=' . $itemPerPage : '';
        }

        $allServices = $this->model->getAllServices();

        return [
            'services'      => $currentServices,
            'prev_page_url' => $prevPageUrl,
            'next_page_url' => $nextPageUrl,
            'total'         => count($allServices),
        ];
    }

    /**
     * Pay order product
     *
     * @param integer $orderProductId
     * @param string $remark
     * @return void
     */
    public function pay(int $orderProductId, string $remark = NULL)
    {
        $result = parent::update([
            'status' => Model::STATUS_PAID,
            'remark' => $remark
        ], ['id' => $orderProductId]);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'order_service_id' => $orderProductId,
            'type' => HistoryModel::TYPE_UPDATE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }

    /**
     * Reject order product
     *
     * @param integer $orderProductId
     * @param string $remark
     * @return void
     */
    public function reject(int $orderProductId, string $remark)
    {
        $result = parent::update([
            'status' => Model::STATUS_REJECTED,
            'remark' => $remark
        ], ['id' => $orderProductId]);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'order_service_id' => $orderProductId,
            'type' => HistoryModel::TYPE_UPDATE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }

    /**
     * Process order product
     *
     * @param integer $orderProductId
     * @param string $remark
     * @return void
     */
    public function process(int $orderProductId, string $remark = NULL)
    {
        $result = parent::update([
            'status' => Model::STATUS_PROCESSED,
            'remark' => $remark
        ], ['id' => $orderProductId]);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'order_service_id' => $orderProductId,
            'type' => HistoryModel::TYPE_UPDATE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }

    /**
     * Complete order product
     *
     * @param integer $orderProductId
     * @param string $remark
     * @return void
     */
    public function complete(int $orderProductId, string $remark = NULL)
    {
        $result = parent::update([
            'status' => Model::STATUS_COMPLETED,
            'remark' => $remark
        ], ['id' => $orderProductId]);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'order_service_id' => $orderProductId,
            'type' => HistoryModel::TYPE_UPDATE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }
}