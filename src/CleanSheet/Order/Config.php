<?php

namespace Cleansheet\Order;

class Config
{
    public static $databaseName = 'order';

    public static $orderRefPrefixes = [
        'name'      => 'INV',
        'module'    => [
            'service'   => 'SRVC',
            'product'   => 'PRDT'
        ]
    ];

    public static $substringStartIndex = 0;
    public static $substringLength = 8;

    public static $productPerPage = 8;
    public static $servicePerPage = 8;
}