<?php

namespace Cleansheet\Order;

use Skeleton\Core\Model as CoreModel;

class Model extends CoreModel
{
    const STATUS_NEW = 'NEW';
    const STATUS_PAID = 'PAID';
    const STATUS_PROCESSED = 'PROCESSED';
    const STATUS_REJECTED = 'REJECTED';
    const STATUS_COMPLETED = 'COMPLETED';
}