<?php

namespace Cleansheet;

abstract class Config
{
    public static $environment = 'development';

    public static $serviceAccount = [];
    public static $bucketName = [];

    public static $headers = [
        'AUTHORIZTATION'    => 'Authorization',
    ];
}