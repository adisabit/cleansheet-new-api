<?php

namespace Cleansheet\Metadata\Contact;

use Cleansheet\Order\Config;
use Skeleton\Core\Model as CoreModel;

class Model extends CoreModel
{    
    /**
     * Order service model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'metadata_contact';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }
}