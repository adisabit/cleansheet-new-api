<?php

namespace Cleansheet\Metadata\Contact;

use Cleansheet\Master\Config;
use Skeleton\Core\Api as CoreApi;

class Api extends CoreApi
{
    /**
     * Contact metadata api constructor
     *
     * @param string $databaseName
     * @param Service $service
     */
    public function __construct(string $databaseName = NULL, Service $service = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->service = $service ?? new Service($this->databaseName);
    }

    /**
     * Get all contacts
     *
     * @return array
     */
    public function list(): array
    {
        $payments = $this->service->find();

        return $payments;
    }

    /**
     * Get by id
     *
     * @param integer $id
     * @return array
     */
    public function retrieve(int $id): array
    {
        return $this->service->getById($id);
    }

    /**
     * Create new contact
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        $result = $this->service->create($data);

        return $result['result'];
    }

    /**
     * Update contact
     *
     * @param array $data
     * @param array $conditions
     * @return int
     */
    public function update(array $data, array $conditions): int
    {
        $result = $this->service->update($data, $conditions);

        return $result['result'];
    }

    /**
     * delete contact
     *
     * @param array $conditions
     * @return int
     */
    public function delete(array $conditions): int
    {
        $result = $this->service->delete($conditions);

        return $result['result'];
    }
}