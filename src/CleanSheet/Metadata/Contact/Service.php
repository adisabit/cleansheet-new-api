<?php

namespace Cleansheet\Metadata\Contact;

use Cleansheet\Master\Config;
use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    /**
     * Master payment service constructor
     *
     * @param string $databaseName
     * @param Model $model
     */
    public function __construct(string $databaseName = NULL,
                                Model $model = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = $model ?? new Model($this->databaseName);
    }
}