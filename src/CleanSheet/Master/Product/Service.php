<?php

namespace Cleansheet\Master\Product;

use Cleansheet\Master\Config;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    /**
     * Product service constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;
        
        $this->model = new Model($this->databaseName);
        $this->historyModel = new HistoryModel($this->databaseName);
    }

    /**
     * Get limit offset
     *
     * @param integer $page
     * @param integer $itemPerPage
     * @return void
     */
    private function getLimitOffset(int $page, int $itemPerPage = NULL)
    {
        $itemPerPage = $itemPerPage ?? Config::$productPerPage;
        $offset = ($page - 1) * $itemPerPage;

        return [
            'limit'    => $itemPerPage,
            'offset'   => $offset
        ];
    }
    
    /**
     * Get all products
     *
     * @return array
     */
    public function getAllProducts(int $page, int $itemPerPage = NULL): array
    {
        $current = $this->getLimitOffset($page, $itemPerPage);
        $prev = $this->getLimitOffset($page - 1, $itemPerPage);
        $next = $this->getLimitOffset($page + 1, $itemPerPage);

        $currentProducts = $current['offset'] < 0 ? [] : $this->model->getAllProducts($current['limit'], $current['offset']);
        $prevProductsExist = $prev['offset'] < 0 ? false : $this->model->areProductsExist($prev['limit'], $prev['offset']);
        $nextProductsExist = $next['offset'] < 0 ? false : $this->model->areProductsExist($next['limit'], $next['offset']);

        $prevPageUrl = NULL;
        if ($prevProductsExist) {
            $prevPageUrl = rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?page=' . ($page - 1);
            $prevPageUrl .= $itemPerPage != NULL ? '&item_per_page=' . $itemPerPage : '';
        }

        $nextPageUrl = NULL;
        if ($nextProductsExist) {
            $nextPageUrl = rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?page=' . ($page + 1);
            $nextPageUrl .= $itemPerPage != NULL ? '&item_per_page=' . $itemPerPage : '';
        }
        return [
            'products'      => $currentProducts,
            'prev_page_url' => $prevPageUrl,
            'next_page_url' => $nextPageUrl
        ];
    }

    /**
     * Create new master product
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        $result = parent::create($data);

        $historyData = array_merge($result['new_data'], [
            'product_id' => $result['result']['id'],
            'type' => HistoryModel::TYPE_INSERT
        ]);

        $this->historyModel->insert($historyData);

        return $result['result'];
    }

    /**
     * Update master product
     *
     * @param array $data
     * @param array $conditions
     * @return array
     */
    public function update(array $data, array $conditions): array
    {
        $product = $this->getOrFail($conditions);

        $result = parent::update($data, $conditions);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'product_id' => $product['id'],
            'type' => HistoryModel::TYPE_UPDATE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }

    /**
     * Delete master product
     *
     * @param array $conditions
     * @return array
     */
    public function delete(array $conditions): array
    {
        $product = $this->getOrFail($conditions);

        $result = parent::delete($conditions);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'product_id' => $product['id'],
            'type' => HistoryModel::TYPE_DELETE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }
}