<?php

namespace Cleansheet\Master\Product;

use Cleansheet\Master\Config;
use Skeleton\Core\Model as CoreModel;
use Skeleton\Core\Exception\DataNotFoundException;

class Model extends CoreModel
{
    /**
     * Product model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'master_product';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }

    /**
     * Get all products
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function getAllProducts(int $limit, int $offset): array
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL LIMIT :limit OFFSET :offset";

        $products = $this->fetch($query, [
            ':limit'    => $limit,
            ':offset'   => $offset
        ]);

        return $products;
    }

    /**
     * Check whether products at current range are exist
     *
     * @param integer $limit
     * @param integer $offset
     * @return bool
     */
    public function areProductsExist(int $limit, int $offset): bool
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL LIMIT :limit OFFSET :offset";

        $products = $this->fetch($query, [
            ':limit'    => $limit,
            ':offset'   => $offset
        ]);

        return count($products) > 0;
    }

    /**
     * Get by id
     *
     * @param integer $id
     * @return array
     */
    public function getById(int $id): array
    {
        try {
            return parent::getById($id);
        } catch (DataNotFoundException $e) {
            throw new DataNotFoundException("Produk tidak ditemukan!");
        }
    }
}