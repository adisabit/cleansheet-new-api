<?php

namespace Cleansheet\Master\Product;

use Cleansheet\Master\Config;
use Skeleton\Core\HistoryModel as CoreHistoryModel;

class HistoryModel extends CoreHistoryModel
{
    /**
     * Product history model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'master_product_history';
    }
}