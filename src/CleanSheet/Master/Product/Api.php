<?php

namespace Cleansheet\Master\Product;

use Cleansheet\Master\Config;
use Skeleton\Core\Api as CoreApi;

class Api extends CoreApi
{
    /**
     * Product api constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;
        
        $this->service = new Service($this->databaseName);
    }

    /**
     * Get all products
     *
     * @param int $page
     * @param int $itemPerPage
     * @return array
     */
    public function getAllProducts(int $page, int $itemPerPage = NULL): array
    {
        $products = $this->service->getAllProducts($page, $itemPerPage);

        return $products;
    }

    /**
     * Create master product
     *
     * @param array $data
     * @return void
     */
    public function create(array $data)
    {
        return $this->service->create($data);
    }

    /**
     * Update master product
     *
     * @param array $data
     * @param array $conditions
     * @return integer
     */
    public function update(array $data, array $conditions): int
    {
        $result = $this->service->update($data, $conditions);

        return $result['result'];
    }

    /**
     * Delete master product
     *
     * @param array $conditions
     * @return integer
     */
    public function delete(array $conditions): int
    {
        $result = $this->service->delete($conditions);

        return $result['result'];
    }

    /**
     * Get master product by its id
     *
     * @param integer $id
     * @return void
     */
    public function getById(int $id): array
    {
        return $this->service->getById($id);
    }
}