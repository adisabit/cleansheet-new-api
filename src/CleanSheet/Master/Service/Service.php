<?php

namespace Cleansheet\Master\Service;

use Cleansheet\Master\Config;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    /**
     * Product service constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = new Model($this->databaseName);
        $this->historyModel = new HistoryModel($this->databaseName);
    }

    /**
     * Get limit offset
     *
     * @param integer $page
     * @param integer $itemPerPage
     * @return void
     */
    private function getLimitOffset(int $page, int $itemPerPage = NULL)
    {
        $itemPerPage = $itemPerPage ?? Config::$servicePerPage;
        $offset = ($page - 1) * $itemPerPage;

        return [
            'limit'    => $itemPerPage,
            'offset'   => $offset
        ];
    }
    /**
     * Get all services
     *
     * @return array
     */
    public function getAllServices(int $page, int $itemPerPage = NULL): array
    {
        $current = $this->getLimitOffset($page, $itemPerPage);
        $prev = $this->getLimitOffset($page - 1, $itemPerPage);
        $next = $this->getLimitOffset($page + 1, $itemPerPage);

        $currentServices = $current['offset'] < 0 ? [] : $this->model->getAllServices($current['limit'], $current['offset']);
        $prevServicesExist = $prev['offset'] < 0 ? false : $this->model->areServicesExist($prev['limit'], $prev['offset']);
        $nextServicesExist = $next['offset'] < 0 ? false : $this->model->areServicesExist($next['limit'], $next['offset']);

        $prevPageUrl = $prevServicesExist ? rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?page=' . ($page - 1) : null;
        $nextPageUrl = $nextServicesExist ? rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?page=' . ($page + 1) : null;
        
        return [
            'services'      => $currentServices,
            'prev_page_url' => $prevPageUrl,
            'next_page_url' => $nextPageUrl
        ];
    }

    /**
     * Get service by category
     *
     * @param string $category
     * @param integer $page
     * @param integer $itemPerPage
     * @return array
     */
    public function findByCategory(string $category, int $page, int $itemPerPage = NULL): array
    {
        $current = $this->getLimitOffset($page, $itemPerPage);
        $prev = $this->getLimitOffset($page - 1, $itemPerPage);
        $next = $this->getLimitOffset($page + 1, $itemPerPage);

        $currentServices = $current['offset'] < 0 ? [] : $this->model->findByCategory($category, $current['limit'], $current['offset']);

        $prevServicesExist = $prev['offset'] < 0 ? false : $this->model->areServicesExist($prev['limit'], $prev['offset'], $category);
        $nextServicesExist = $next['offset'] < 0 ? false : $this->model->areServicesExist($next['limit'], $next['offset'], $category);

        $prevPageUrl = NULL;
        if ($prevServicesExist) {
            $prevPageUrl = rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?category=' . $category . '&page=' . ($page - 1);
            $prevPageUrl .= $itemPerPage != NULL ? '&item_per_page=' . $itemPerPage : '';
        }

        $nextPageUrl = NULL;
        if ($nextServicesExist) {
            $nextPageUrl = rtrim(Request::getBaseUrl(), '/') . preg_replace('/\?.*/', '', Request::getUrl()) . '?category=' . $category . '&page=' . ($page + 1);
            $nextPageUrl .= $itemPerPage != NULL ? '&item_per_page=' . $itemPerPage : '';
        }
        
        return [
            'services'      => $currentServices,
            'prev_page_url' => $prevPageUrl,
            'next_page_url' => $nextPageUrl
        ];
    }

    /**
     * Create new service
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        $service = parent::create($data);

        $historyData = array_merge($service['new_data'], [
            'service_id'    => $service['result']['id'],
            'type'          => HistoryModel::TYPE_INSERT
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result'    => $service['result']
        ];
    }

    /**
     * Update master service
     *
     * @param array $data
     * @param array $conditions
     * @return array
     */
    public function update(array $data, array $conditions): array
    {
        $result = parent::update($data, $conditions);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'service_id'    => $conditions['id'],
            'type'          => HistoryModel::TYPE_UPDATE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result'    => $result['result']
        ];
    }

    /**
     * Delete master service
     *
     * @param array $conditions
     * @return array
     */
    public function delete(array $conditions): array
    {
        $result = parent::delete($conditions);

        $historyData = [
            'service_id'    => $conditions['id'],
            'type'          => HistoryModel::TYPE_DELETE
        ];

        $this->historyModel->insert($historyData);

        return [
            'result'    => $result['result']
        ];
    }
}