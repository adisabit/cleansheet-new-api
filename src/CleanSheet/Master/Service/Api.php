<?php

namespace Cleansheet\Master\Service;

use Cleansheet\Master\Config;
use Skeleton\Core\Api as CoreApi;

class Api extends CoreApi
{
    /**
     * Product api constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->service = new Service($this->databaseName);
    }

    /**
     * Get all services
     *
     * @return array
     */
    public function getAllServices(int $page, int $itemPerPage = NULL): array
    {
        $services = $this->service->getAllServices($page, $itemPerPage);

        return $services;
    }

    /**
     * Get service by category
     *
     * @param string $category
     * @param integer $page
     * @param integer $itemPerPage
     * @return array
     */
    public function findByCategory(string $category, int $page, int $itemPerPage = NULL): array
    {
        $services = $this->service->findByCategory($category, $page, $itemPerPage);

        return $services;
    }

    /**
     * Get master service by id
     *
     * @param integer $id
     * @return array
     */
    public function getById(int $id): array
    {
        return $this->service->getById($id);
    }

    /**
     * Create new service
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        $service = $this->service->create($data);

        return $service['result'];
    }

    /**
     * Update service
     *
     * @param array $data
     * @param array $conditions
     * @return integer
     */
    public function update(array $data, array $conditions): int
    {
        $result = $this->service->update($data, $conditions);

        return $result['result'];
    }

    /**
     * Delete service
     *
     * @param array $conditions
     * @return integer
     */
    public function delete(array $conditions): int
    {
        $result = $this->service->delete($conditions);

        return $result['result'];
    }
}