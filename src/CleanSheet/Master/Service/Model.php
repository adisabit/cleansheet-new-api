<?php

namespace Cleansheet\Master\Service;

use Cleansheet\Master\Config;
use Skeleton\Core\Model as CoreModel;

class Model extends CoreModel
{
    /**
     * Service model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'master_service';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }

    /**
     * Get all services
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function getAllServices(int $limit, int $offset): array
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL LIMIT :limit OFFSET :offset";

        $services = $this->fetch($query, [
            ':limit'    => $limit,
            ':offset'   => $offset
        ]);

        return $services;
    }

    /**
     * Check whether services at current range are exist
     *
     * @param integer $limit
     * @param integer $offset
     * @return bool
     */
    public function areServicesExist(int $limit, int $offset, string $category = NULL): bool
    {
        if ($category == NULL) {
            $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL LIMIT :limit OFFSET :offset";

            $services = $this->fetch($query, [
                ':limit'    => $limit,
                ':offset'   => $offset
            ]);
        } else {
            $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL AND category = :category LIMIT :limit OFFSET :offset";

            $services = $this->fetch($query, [
                ':category' => $category,
                ':limit'    => $limit,
                ':offset'   => $offset
            ]);
        }

        return count($services) > 0;
    }

    /**
     * FInd by category
     *
     * @param string $category
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function findByCategory(string $category, int $limit, int $offset): array
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL AND category = :category LIMIT :limit OFFSET :offset";

        $services = $this->fetch($query, [
            ':category' => $category,
            ':limit'    => $limit,
            ':offset'   => $offset
        ]);

        return $services;
    }
}