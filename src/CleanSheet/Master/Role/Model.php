<?php

namespace Cleansheet\Master\Role;

use Cleansheet\Master\Config;
use Skeleton\Core\Model as CoreModel;

class Model extends CoreModel
{
    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;
    
    /**
     * Master role model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'master_role';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }
}