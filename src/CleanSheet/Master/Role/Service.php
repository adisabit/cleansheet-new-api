<?php

namespace Cleansheet\Master\Role;

use Cleansheet\Master\Config;
use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    /**
     * Master role service constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = new Model($this->databaseName);
        $this->historyModel = new HistoryModel($this->databaseName);
    }

    /**
     * Get all roles
     *
     * @return array
     */
    public function getAllRoles(): array
    {
        $roles = $this->model->find();

        return $roles;
    }
}