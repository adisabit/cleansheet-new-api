<?php

namespace Cleansheet\Master\Role;

use Cleansheet\Master\Config;
use Skeleton\Core\HistoryModel as CoreHistoryModel;

class HistoryModel extends CoreHistoryModel
{
    /**
     * Master role history model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'master_role_history';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }
}