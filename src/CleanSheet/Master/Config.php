<?php

namespace Cleansheet\Master;

class Config
{
    public static $databaseName = 'master';

    public static $productPerPage = 8;
    public static $servicePerPage = 8;
}