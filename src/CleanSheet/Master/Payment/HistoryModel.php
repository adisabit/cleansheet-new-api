<?php

namespace Cleansheet\Master\Payment;

use Cleansheet\Master\Config;
use Skeleton\Core\HistoryModel as CoreHistoryModel;

class HistoryModel extends CoreHistoryModel
{
    /**
     * Payment history model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'master_payment_history';
    }
}