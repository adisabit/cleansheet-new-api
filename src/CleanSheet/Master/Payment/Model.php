<?php

namespace Cleansheet\Master\Payment;

use Cleansheet\Master\Config;
use Skeleton\Core\Model as CoreModel;

class Model extends CoreModel
{
    /**
     * Master payment model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'master_payment';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }
}