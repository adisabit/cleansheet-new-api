<?php

namespace Cleansheet\Master\Payment;

use Cleansheet\Master\Config;
use Skeleton\Core\Api as CoreApi;

class Api extends CoreApi
{
    /**
     * Master payment api constructor
     *
     * @param string $databaseName
     * @param Service $service
     */
    public function __construct(string $databaseName = NULL, Service $service = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->service = $service ?? new Service($this->databaseName);
    }

    /**
     * Get all payments
     *
     * @return array
     */
    public function getAllPayments(): array
    {
        $payments = $this->service->getAllPayments();

        return $payments;
    }

    /**
     * Get by id
     *
     * @param integer $id
     * @return array
     */
    public function getById(int $id): array
    {
        return $this->service->getById($id);
    }

    /**
     * Create new payment method
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        return $this->service->create($data);
    }

    /**
     * Update payment method
     *
     * @param array $data
     * @param array $conditions
     * @return int
     */
    public function update(array $data, array $conditions): int
    {
        $result = $this->service->update($data, $conditions);

        return $result['result'];
    }

    /**
     * delete payment method
     *
     * @param array $conditions
     * @return int
     */
    public function delete(array $conditions): int
    {
        $result = $this->service->delete($conditions);

        return $result['result'];
    }
}