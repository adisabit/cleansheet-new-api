<?php

namespace Cleansheet\Master\Payment;

use Cleansheet\Master\Config;
use Skeleton\Core\Service as CoreService;

class Service extends CoreService
{
    /**
     * Master payment service constructor
     *
     * @param string $databaseName
     * @param Model $model
     */
    public function __construct(string $databaseName = NULL,
                                Model $model = NULL,
                                HistoryModel $historyModel = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->model = $model ?? new Model($this->databaseName);
        $this->historyModel = $historyModel ?? new HistoryModel($this->databaseName);
    }

    /**
     * Get all available payments
     *
     * @return void
     */
    public function getAllPayments()
    {
        $payments = $this->model->find();

        return $payments;
    }

    /**
     * Create new payment method
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        $result = parent::create($data);

        $historyData = array_merge($result['new_data'], [
            'payment_id' => $result['result']['id'],
            'type' => HistoryModel::TYPE_INSERT
        ]);

        $this->historyModel->insert($historyData);

        return $result['result'];
    }

    /**
     * Update payment method
     *
     * @param array $data
     * @param array $conditions
     * @return array
     */
    public function update(array $data, array $conditions): array
    {
        $result = parent::update($data, $conditions);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'payment_id' => $conditions['id'],
            'type' => HistoryModel::TYPE_UPDATE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }

    /**
     * Delete payment method
     *
     * @param array $conditions
     * @return array
     */
    public function delete(array $conditions): array
    {
        $result = parent::delete($conditions);

        $historyData = array_merge($result['old_data'], $result['new_data'], [
            'payment_id' => $conditions['id'],
            'type' => HistoryModel::TYPE_DELETE
        ]);

        $this->historyModel->insert($historyData);

        return [
            'result' => $result['result']
        ];
    }
}