<?php

namespace Cleansheet\Registrant;

abstract class Config
{
    public static $databaseName = 'registrant';

    public static $registrantPerPage = 10;
}