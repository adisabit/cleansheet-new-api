<?php

namespace Cleansheet\Registrant;

use Skeleton\Core\Api as CoreApi;

class Api extends CoreApi
{
    /**
     * Registrant api constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        $this->databaseName = $databaseName ?? Config::$databaseName;

        $this->service = new Service($this->databaseName);
    }

    /**
     * Create new registrant
     *
     * @param array $data
     * @return array
     */
    public function create(array $data): array
    {
        $result = $this->service->create($data);

        return $result['result'];
    }

    /**
     * Update registrant
     *
     * @param array $data
     * @param array $conditions
     * @return int
     */
    public function update(array $data, array $conditions): int
    {
        $result = $this->service->update($data, $conditions);

        return $result['result'];
    }

    /**
     * Delete registrant
     *
     * @param array $conditions
     * @return int
     */
    public function delete(array $conditions): int
    {
        $result = $this->service->delete($conditions);

        return $result['result'];
    }

    /**
     * Get registrant by id
     *
     * @param integer $id
     * @return array
     */
    public function getById(int $id): array
    {
        $registrant = $this->service->getById($id);

        return $registrant;
    }

    /**
     * Find registrants
     *
     * @param integer $page
     * @param integer $itemPerPage
     * @return array
     */
    public function findRegistrants(int $page, int $itemPerPage = NULL): array
    {
        $registrants = $this->service->findRegistrants($page, $itemPerPage);

        return $registrants;
    }
}