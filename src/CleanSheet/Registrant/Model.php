<?php

namespace Cleansheet\Registrant;

use Skeleton\Core\Model as CoreModel;

class Model extends CoreModel
{
    /**
     * Registrant model constructor
     *
     * @param string $databaseName
     */
    public function __construct(string $databaseName = NULL)
    {
        parent::__construct($databaseName ?? Config::$databaseName);

        $this->tableName = 'registrant';
        $this->excludes = ['created_time', 'updated_time', 'deleted_time'];
    }

    /**
     * Check whether registrant exist
     *
     * @param integer $limit
     * @param integer $offset
     * @return boolean
     */
    public function areRegistrantExists(int $limit, int $offset): bool
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL LIMIT :limit OFFSET :offset";

        $registrants = $this->fetch($query, [
            ':limit'    => $limit,
            ':offset'   => $offset
        ]);

        return count($registrants) > 0;
    }

    /**
     * Find registrants
     *
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function findRegistrants(int $limit, int $offset): array
    {
        $query = "SELECT * FROM {$this->tableName} WHERE deleted_time IS NULL LIMIT :limit OFFSET :offset";

        $registrants = $this->fetch($query, [
            ':limit'    => $limit,
            ':offset'   => $offset
        ]);

        return $registrants;
    }
}