<?php

use Cleansheet\Config;
use Skeleton\Core\Library\Environment;

Config::$environment = Environment::get("CLEANSHEET_ENV", 'development');
