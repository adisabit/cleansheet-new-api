<?php

use Skeleton\Core\Library\Database;
use Skeleton\Core\Library\Environment;

Database::$databases = [
    'user'          => Environment::get('CLEANSHEET_DB_USER', 'mysql://root:@localhost:3306/cleansheet_new'),
    'core'          => Environment::get('SKELETON_DB_CORE', 'mysql://root:@localhost:3306/cleansheet_new'),
    'master'        => Environment::get('CLEANSHEET_DB_MASTER', 'mysql://root:@localhost:3306/cleansheet_new'),
    'order'         => Environment::get('CLEANSHEET_DB_ORDER', 'mysql://root:@localhost:3306/cleansheet_new'),
    'registrant'    => Environment::get('CLEANSHEET_DB_REGISTRANT', 'mysql://root:@localhost:3306/cleansheet_new'),
    'partner'       => Environment::get('CLEANSHEET_DB_PARTNER', 'mysql://root:@localhost:3306/cleansheet_new'),
];