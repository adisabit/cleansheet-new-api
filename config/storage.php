<?php

use Cleansheet\Config;
use Skeleton\Core\Library\Environment;

Config::$serviceAccount = [
    'development' => [
        "type"                          => "service_account",
        "project_id"                    => Environment::get('FIREBASE_PROJECT_ID_DEV', "cleansheet-dev"),
        "private_key_id"                => Environment::get('FIREBASE_PRIVATE_KEY_ID_DEV', "4eccf812137fe7a4302582711885d1be67abb3e3"),
        "private_key"                   => Environment::get('FIREBASE_PRIVATE_KEY_DEV', "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDRgxkMAXsWPlWv\nC6OqDmzrUemKhgzmmi5PE9Zq1R+F0kNobJeT8dPe06eqFo/b+QZTQ1aAxm4g1suU\nDXNRhZRl6Hk0IDukQRQo/RwLz0b4Sw4GdoaC6RUorT00+OBAgQOBv+uMmXZhHfMs\nQuMWsaduZTqtKlRX/V+UjWQ/v6jhHSNZVqgB7K4NLPVmvor8VLhwtQLjXAV03Ls1\nabKoKkvpe1WxPoOD3rsOMEYhqZy2T37qQ1f9uCbe6Pdd49dmP18We53nr/qJpxnV\ngauWlQdXkar6mDq3LafrI8K7/AQhgSrEKutMbBWLe3JjvWrp2bU7hx1XU7clG6zc\n5O0wJz5bAgMBAAECggEAE1Z3XF1SoBX20KZRX+Pe98Viue9vh2VZuuDI3Kl5NF/Y\n6u2C/BvJA3sjsZZNhu8wP0Uq2c324j8RIUQwjai3rsRLU51aA+oh0zqxeQGTul0r\nGTudZL3gvxVmXdpO0IVX7oTpV9veSk56YG54CWMyYgUj6lHK4Sg4xzxZsWyeSNC9\n1KdTTkw/7drqsBuvDB18NQ15qEd+h9lLSTE4ykFeMzH+kUiS/r7y1Ay77FYqZOUU\nBbQ27d5fr7ZRDNDTkBrmrJTJV9BIcpMACVxkrQSCBneAJlkaUsHeXk+Nigky0viU\nKt2uYT+yM3hFdlAwNDkIyRv6AbTnQWY4eeFnQ4Lk2QKBgQD4N1E+Ci9HL5m9M5jP\nXMasBB3HGBYdMpwwD5MzijyL5CATymAePqpiPKTFwpafC95g80/0iU+1a3hmw30i\n9yaqN6BO5WBE8gcbWtRNHwOYA3gRRzw/pWxqjzvq6C/XQUnQ+4XMEc49RLyUmsEx\nMKIWn4Kznw7KRaoVBfqOCmtfNQKBgQDYFRB2ITu4dMCbZjaR8g7x3bp0+uuPU+hM\naE2BCyGooshULDoX760QiMVwqtM5dn5zobTE1PjG2eYZt7wijmT6d969ZjRxi8xn\nwjlSugvz1jcHnWJM7Ja+eRFzdY9H/xfxUQ1ZK2C21fCjhs9X4YoEroWHkc2ftXDr\n166xYXwJTwKBgCMFsxnc2ATkS33trM79WS7mOclgtaYTAQSq/esUwyw92OTR9jPu\nRZ4szlb0+MmiJBNr4UkwoqWHb2qYhKyNUNBNnvni1W2E9MnATBpBmQE25BcUBQU5\n8w0p/ptLfYySsrag9qTvkjkE3NLe7wlgwk4M3aU4eR7DK5dhy5OFFtdNAoGAUb0O\nIBGMPMd1gJNWuzicrKOaf9vXNb2g1x5xiwoB7n+9VhHw29MfXWzPe0Izar5HMUiP\nUBHcFaShpULamRvSWOj1SiIaXoVqiilvc7rLAK8c2/WOFNk+YMj9b/adf7egf/mQ\nGhYNnLwrLkC1uGGpywGVo2XlKbACCkTpYVImEIMCgYEAnerbpRTerey1gafcVac/\nhY+LuzQE99X57GPk0qTrsRtKuqxLFis41RwuBWEH46vo0humwNv240zkrA0CVAYm\nYRFSGqXaSI5qBqYtgATEA6Ho0NqBRAA735ujHgQ1wCDkUFrVnaueogGHCRqwtjle\n7qAMFs9jo2aSYPpdkXsTHGs=\n-----END PRIVATE KEY-----\n"),
        "client_email"                  => Environment::get('FIREBASE_CLIENT_EMAIL_DEV', "firebase-adminsdk-46i90@cleansheet-dev.iam.gserviceaccount.com"),
        "client_id"                     => Environment::get('FIREBASE_CLIENT_ID', "110750045541738269420"),
        "auth_uri"                      => "https://accounts.google.com/o/oauth2/auth",
        "token_uri"                     => "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url"   => "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url"          => Environment::get('FIREBASE_CLIENT_X509_CERT_URL_DEV', "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-46i90%40cleansheet-dev.iam.gserviceaccount.com")
    ],
    'staging' => [
        "type"                          => "service_account",
        "project_id"                    => Environment::get('FIREBASE_PROJECT_ID_STAGING', "cleansheet-dev"),
        "private_key_id"                => Environment::get('FIREBASE_PRIVATE_KEY_ID_STAGING', "4eccf812137fe7a4302582711885d1be67abb3e3"),
        "private_key"                   => Environment::get('FIREBASE_PRIVATE_KEY_STAGING', "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDRgxkMAXsWPlWv\nC6OqDmzrUemKhgzmmi5PE9Zq1R+F0kNobJeT8dPe06eqFo/b+QZTQ1aAxm4g1suU\nDXNRhZRl6Hk0IDukQRQo/RwLz0b4Sw4GdoaC6RUorT00+OBAgQOBv+uMmXZhHfMs\nQuMWsaduZTqtKlRX/V+UjWQ/v6jhHSNZVqgB7K4NLPVmvor8VLhwtQLjXAV03Ls1\nabKoKkvpe1WxPoOD3rsOMEYhqZy2T37qQ1f9uCbe6Pdd49dmP18We53nr/qJpxnV\ngauWlQdXkar6mDq3LafrI8K7/AQhgSrEKutMbBWLe3JjvWrp2bU7hx1XU7clG6zc\n5O0wJz5bAgMBAAECggEAE1Z3XF1SoBX20KZRX+Pe98Viue9vh2VZuuDI3Kl5NF/Y\n6u2C/BvJA3sjsZZNhu8wP0Uq2c324j8RIUQwjai3rsRLU51aA+oh0zqxeQGTul0r\nGTudZL3gvxVmXdpO0IVX7oTpV9veSk56YG54CWMyYgUj6lHK4Sg4xzxZsWyeSNC9\n1KdTTkw/7drqsBuvDB18NQ15qEd+h9lLSTE4ykFeMzH+kUiS/r7y1Ay77FYqZOUU\nBbQ27d5fr7ZRDNDTkBrmrJTJV9BIcpMACVxkrQSCBneAJlkaUsHeXk+Nigky0viU\nKt2uYT+yM3hFdlAwNDkIyRv6AbTnQWY4eeFnQ4Lk2QKBgQD4N1E+Ci9HL5m9M5jP\nXMasBB3HGBYdMpwwD5MzijyL5CATymAePqpiPKTFwpafC95g80/0iU+1a3hmw30i\n9yaqN6BO5WBE8gcbWtRNHwOYA3gRRzw/pWxqjzvq6C/XQUnQ+4XMEc49RLyUmsEx\nMKIWn4Kznw7KRaoVBfqOCmtfNQKBgQDYFRB2ITu4dMCbZjaR8g7x3bp0+uuPU+hM\naE2BCyGooshULDoX760QiMVwqtM5dn5zobTE1PjG2eYZt7wijmT6d969ZjRxi8xn\nwjlSugvz1jcHnWJM7Ja+eRFzdY9H/xfxUQ1ZK2C21fCjhs9X4YoEroWHkc2ftXDr\n166xYXwJTwKBgCMFsxnc2ATkS33trM79WS7mOclgtaYTAQSq/esUwyw92OTR9jPu\nRZ4szlb0+MmiJBNr4UkwoqWHb2qYhKyNUNBNnvni1W2E9MnATBpBmQE25BcUBQU5\n8w0p/ptLfYySsrag9qTvkjkE3NLe7wlgwk4M3aU4eR7DK5dhy5OFFtdNAoGAUb0O\nIBGMPMd1gJNWuzicrKOaf9vXNb2g1x5xiwoB7n+9VhHw29MfXWzPe0Izar5HMUiP\nUBHcFaShpULamRvSWOj1SiIaXoVqiilvc7rLAK8c2/WOFNk+YMj9b/adf7egf/mQ\nGhYNnLwrLkC1uGGpywGVo2XlKbACCkTpYVImEIMCgYEAnerbpRTerey1gafcVac/\nhY+LuzQE99X57GPk0qTrsRtKuqxLFis41RwuBWEH46vo0humwNv240zkrA0CVAYm\nYRFSGqXaSI5qBqYtgATEA6Ho0NqBRAA735ujHgQ1wCDkUFrVnaueogGHCRqwtjle\n7qAMFs9jo2aSYPpdkXsTHGs=\n-----END PRIVATE KEY-----\n"),
        "client_email"                  => Environment::get('FIREBASE_CLIENT_EMAIL_STAGING', "firebase-adminsdk-46i90@cleansheet-dev.iam.gserviceaccount.com"),
        "client_id"                     => Environment::get('FIREBASE_CLIENT_ID', "110750045541738269420"),
        "auth_uri"                      => "https://accounts.google.com/o/oauth2/auth",
        "token_uri"                     => "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url"   => "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url"          => Environment::get('FIREBASE_CLIENT_X509_CERT_URL_STAGING', "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-46i90%40cleansheet-dev.iam.gserviceaccount.com")
    ],
    'production' => [
        "type"                          => "service_account",
        "project_id"                    => Environment::get('FIREBASE_PROJECT_ID_PROD', "cleansheet-dev"),
        "private_key_id"                => Environment::get('FIREBASE_PRIVATE_KEY_ID_PROD', "4eccf812137fe7a4302582711885d1be67abb3e3"),
        "private_key"                   => Environment::get('FIREBASE_PRIVATE_KEY_PROD', "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDRgxkMAXsWPlWv\nC6OqDmzrUemKhgzmmi5PE9Zq1R+F0kNobJeT8dPe06eqFo/b+QZTQ1aAxm4g1suU\nDXNRhZRl6Hk0IDukQRQo/RwLz0b4Sw4GdoaC6RUorT00+OBAgQOBv+uMmXZhHfMs\nQuMWsaduZTqtKlRX/V+UjWQ/v6jhHSNZVqgB7K4NLPVmvor8VLhwtQLjXAV03Ls1\nabKoKkvpe1WxPoOD3rsOMEYhqZy2T37qQ1f9uCbe6Pdd49dmP18We53nr/qJpxnV\ngauWlQdXkar6mDq3LafrI8K7/AQhgSrEKutMbBWLe3JjvWrp2bU7hx1XU7clG6zc\n5O0wJz5bAgMBAAECggEAE1Z3XF1SoBX20KZRX+Pe98Viue9vh2VZuuDI3Kl5NF/Y\n6u2C/BvJA3sjsZZNhu8wP0Uq2c324j8RIUQwjai3rsRLU51aA+oh0zqxeQGTul0r\nGTudZL3gvxVmXdpO0IVX7oTpV9veSk56YG54CWMyYgUj6lHK4Sg4xzxZsWyeSNC9\n1KdTTkw/7drqsBuvDB18NQ15qEd+h9lLSTE4ykFeMzH+kUiS/r7y1Ay77FYqZOUU\nBbQ27d5fr7ZRDNDTkBrmrJTJV9BIcpMACVxkrQSCBneAJlkaUsHeXk+Nigky0viU\nKt2uYT+yM3hFdlAwNDkIyRv6AbTnQWY4eeFnQ4Lk2QKBgQD4N1E+Ci9HL5m9M5jP\nXMasBB3HGBYdMpwwD5MzijyL5CATymAePqpiPKTFwpafC95g80/0iU+1a3hmw30i\n9yaqN6BO5WBE8gcbWtRNHwOYA3gRRzw/pWxqjzvq6C/XQUnQ+4XMEc49RLyUmsEx\nMKIWn4Kznw7KRaoVBfqOCmtfNQKBgQDYFRB2ITu4dMCbZjaR8g7x3bp0+uuPU+hM\naE2BCyGooshULDoX760QiMVwqtM5dn5zobTE1PjG2eYZt7wijmT6d969ZjRxi8xn\nwjlSugvz1jcHnWJM7Ja+eRFzdY9H/xfxUQ1ZK2C21fCjhs9X4YoEroWHkc2ftXDr\n166xYXwJTwKBgCMFsxnc2ATkS33trM79WS7mOclgtaYTAQSq/esUwyw92OTR9jPu\nRZ4szlb0+MmiJBNr4UkwoqWHb2qYhKyNUNBNnvni1W2E9MnATBpBmQE25BcUBQU5\n8w0p/ptLfYySsrag9qTvkjkE3NLe7wlgwk4M3aU4eR7DK5dhy5OFFtdNAoGAUb0O\nIBGMPMd1gJNWuzicrKOaf9vXNb2g1x5xiwoB7n+9VhHw29MfXWzPe0Izar5HMUiP\nUBHcFaShpULamRvSWOj1SiIaXoVqiilvc7rLAK8c2/WOFNk+YMj9b/adf7egf/mQ\nGhYNnLwrLkC1uGGpywGVo2XlKbACCkTpYVImEIMCgYEAnerbpRTerey1gafcVac/\nhY+LuzQE99X57GPk0qTrsRtKuqxLFis41RwuBWEH46vo0humwNv240zkrA0CVAYm\nYRFSGqXaSI5qBqYtgATEA6Ho0NqBRAA735ujHgQ1wCDkUFrVnaueogGHCRqwtjle\n7qAMFs9jo2aSYPpdkXsTHGs=\n-----END PRIVATE KEY-----\n"),
        "client_email"                  => Environment::get('FIREBASE_CLIENT_EMAIL_PROD', "firebase-adminsdk-46i90@cleansheet-dev.iam.gserviceaccount.com"),
        "client_id"                     => Environment::get('FIREBASE_CLIENT_ID', "110750045541738269420"),
        "auth_uri"                      => "https://accounts.google.com/o/oauth2/auth",
        "token_uri"                     => "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url"   => "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url"          => Environment::get('FIREBASE_CLIENT_X509_CERT_URL_PROD', "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-46i90%40cleansheet-dev.iam.gserviceaccount.com")
    ],
];

Config::$bucketName = [
    'development'   => Environment::get("FIREBASE_BUCKET_NAME_DEV", 'cleansheet-dev.appspot.com'),
    'staging'       => Environment::get("FIREBASE_BUCKET_NAME_STAGING", 'cleansheet-dev.appspot.com'),
    'production'    => Environment::get("FIREBASE_BUCKET_NAME_PRODUCTION", 'cleansheet-dev.appspot.com'),
];