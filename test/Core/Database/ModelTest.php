<?php

namespace Skeleton\Test\Core\Database;

use PDO;
use PDOException;
use PDOStatement;
use PHPUnit\Framework\TestCase;
use Skeleton\Core\Database\Model;

class ModelTest extends TestCase
{
    private $mockData = [
        'id' => 1,
        'created_time' => '2020-02-15 08:20:00',
        'updated_time' => NULL,
        'deleted_time' => NULL,
    ];

    /**
     * Setup before test
     *
     * @return void
     */
    protected function setup(): void
    {
        \Skeleton\Core\Library\Database::$instances = [
            'test' => [
                'driver'    => \Skeleton\Core\Library\Environment::get('DB_TEST_DRIVER', 'mysql'),
                'host'      => \Skeleton\Core\Library\Environment::get('DB_TEST_HOST', 'localhost'),
                'port'      => \Skeleton\Core\Library\Environment::get('DB_TEST_PORT', 3306),
                'database'  => \Skeleton\Core\Library\Environment::get('DB_TEST_DATABASE', 'skeleton'),
                'username'  => \Skeleton\Core\Library\Environment::get('DB_TEST_USERNAME', 'skeleton'),
                'password'  => \Skeleton\Core\Library\Environment::get('DB_TEST_PASSWORD', 'skeleton'),
            ]
        ];
    }

    /**
     * Test fetch in case error while execute then it should thow PDOException
     *
     * @return void
     */
    public function testFetch_inCaseErrorWhileExecute_itShouldThrowPDOException(): void
    {
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('bindValue')->willReturn(true);
        $statementMock->method('execute')->willReturn(false);
        $statementMock->method('errorInfo')->willReturn([1, 2, 'Error occured']);

        $pdoMock = $this->createMock(PDO::class);
        $pdoMock->method('prepare')->willReturn($statementMock);

        $this->expectException(PDOException::class);
        $this->expectExceptionCode(1);
        $this->expectExceptionMessage('Error occured');

        $model = new Model('test');
        $model->tableName = 'test';
        $model->setPdo($pdoMock);

        $model->fetch("select * from {$model->tableName}", []);
    }

    /**
     * Test fetch in case execute is success and excludes field is not empty then it should
     * return the data
     *
     * @return void
     */
    public function testFetch_inCaseExecuteIsSuccessAndExcludesFieldIsNotEmpty_itShouldReturnTheData(): void
    {
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('bindValue')->willReturn(true);
        $statementMock->method('execute')->willReturn(true);
        $statementMock->method('fetchAll')->willReturn([$this->mockData, $this->mockData]);

        $pdoMock = $this->createMock(PDO::class);
        $pdoMock->method('prepare')->willReturn($statementMock);

        $model = new Model('test');
        $model->tableName = 'test';
        $model->excludes = ['created_time', 'updated_time', 'deleted_time'];
        $model->setPdo($pdoMock);

        $result = $model->fetch("select * from {$model->tableName}", []);

        $this->assertEquals(2, count($result));
        $this->assertEquals(['id'], array_keys($result[0]));
    }

    /**
     * Test fetch in case execute is success and includes field is not empty then it should
     * return the data
     *
     * @return void
     */
    public function testFetch_inCaseExecuteIsSuccessAndIncludesFieldIsNotEmpty_itShouldReturnTheData(): void
    {
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('bindValue')->willReturn(true);
        $statementMock->method('execute')->willReturn(true);
        $statementMock->method('fetchAll')->willReturn([$this->mockData, $this->mockData]);

        $pdoMock = $this->createMock(PDO::class);
        $pdoMock->method('prepare')->willReturn($statementMock);

        $model = new Model('test');
        $model->tableName = 'test';
        $model->includes = ['id'];
        $model->setPdo($pdoMock);

        $result = $model->fetch("select * from {$model->tableName}", []);

        $this->assertEquals(2, count($result));
        $this->assertEquals(['id'], array_keys($result[0]));
    }

    /**
     * Test fetch in case execure is success and includes excludes filed is not empty then it should
     * return the data
     *
     * @return void
     */
    public function testFetch_inCaseExecuteIsSuccessAndIncludesExcludesFieldIsNotEmpty_itShouldReturnTheData(): void
    {
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('bindValue')->willReturn(true);
        $statementMock->method('execute')->willReturn(true);
        $statementMock->method('fetchAll')->willReturn([$this->mockData, $this->mockData]);

        $pdoMock = $this->createMock(PDO::class);
        $pdoMock->method('prepare')->willReturn($statementMock);

        $model = new Model('test');
        $model->tableName = 'test';
        $model->includes = ['id'];
        $model->excludes = ['created_time', 'updated_time', 'deleted_time'];
        $model->setPdo($pdoMock);

        $result = $model->fetch("select * from {$model->tableName}", []);

        $this->assertEquals(2, count($result));
        $this->assertEquals(['id'], array_keys($result[0]));
    }

    /**
     * Test fetch in execute is success and includes excludes field is empty then it should
     * return the data
     *
     * @return void
     */
    public function testFetch_inCaseExecuteIsSuccessAndIncludesExcludesFieldIsEmpty_itShouldReturnTheData(): void
    {
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('bindValue')->willReturn(true);
        $statementMock->method('execute')->willReturn(true);
        $statementMock->method('fetchAll')->willReturn([$this->mockData, $this->mockData]);

        $pdoMock = $this->createMock(PDO::class);
        $pdoMock->method('prepare')->willReturn($statementMock);

        $model = new Model('test');
        $model->tableName = 'test';
        $model->setPdo($pdoMock);

        $result = $model->fetch("select * from {$model->tableName}", []);

        $this->assertEquals(2, count($result));
        $this->assertEquals(['id', 'created_time', 'updated_time', 'deleted_time'], array_keys($result[0]));
    }

    /**
     * Test insert in case error while execute then it should thow PDOException
     *
     * @return void
     */
    public function testInsert_inCaseErrorWhileExecute_itShouldThrowPDOException(): void
    {
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('bindValue')->willReturn(true);
        $statementMock->method('execute')->willReturn(false);
        $statementMock->method('errorInfo')->willReturn([1, 2, 'Error occured']);

        $pdoMock = $this->createMock(PDO::class);
        $pdoMock->method('prepare')->willReturn($statementMock);

        $this->expectException(PDOException::class);
        $this->expectExceptionCode(1);
        $this->expectExceptionMessage('Error occured');

        $model = new Model('test');
        $model->tableName = 'test';
        $model->setPdo($pdoMock);

        $model->insert([ 'id' => 1 ]);
    }

    /**
     * Test insert in execute is success then it should return the inserted data
     *
     * @return void
     */
    public function testInsert_inCaseExecuteIsSuccess_itShouldReturnTheInsertedData(): void
    {
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('bindValue')->willReturn(true);
        $statementMock->method('execute')->willReturn(true);
        $statementMock->method('fetchAll')->willReturn([$this->mockData]);

        $pdoMock = $this->createMock(PDO::class);
        $pdoMock->method('prepare')->willReturn($statementMock);

        $model = new Model('test');
        $model->tableName = 'test';
        $model->setPdo($pdoMock);

        $result = $model->insert([ 'id' => 1 ]);

        $this->assertEquals(array_keys($this->mockData), array_keys($result));
    }

    /**
     * Test update in case error while execute then it should thow PDOException
     *
     * @return void
     */
    public function testUpdate_inCaseErrorWhileExecute_itShouldThrowPDOException(): void
    {
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('bindValue')->willReturn(true);
        $statementMock->method('execute')->willReturn(false);
        $statementMock->method('errorInfo')->willReturn([1, 2, 'Error occured']);

        $pdoMock = $this->createMock(PDO::class);
        $pdoMock->method('prepare')->willReturn($statementMock);

        $this->expectException(PDOException::class);
        $this->expectExceptionCode(1);
        $this->expectExceptionMessage('Error occured');

        $model = new Model('test');
        $model->tableName = 'test';
        $model->setPdo($pdoMock);

        $model->update(['id' => 100], ['id' => 1]);
    }

    /**
     * Test update in execute is success then it should return the total of updated data
     *
     * @return void
     */
    public function testUpdate_inCaseExecuteIsSuccess_itShouldReturnTheTotalOfUpdatedData(): void
    {
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('bindValue')->willReturn(true);
        $statementMock->method('execute')->willReturn(true);
        $statementMock->method('rowCount')->willReturn(1);

        $pdoMock = $this->createMock(PDO::class);
        $pdoMock->method('prepare')->willReturn($statementMock);

        $model = new Model('test');
        $model->tableName = 'test';
        $model->setPdo($pdoMock);

        $result = $model->update(['id' => 100], ['id' => 1]);

        $this->assertEquals(1, $result);
    }

    /**
     * Test delete in case error while execute then it should thow PDOException
     *
     * @return void
     */
    public function testDelete_inCaseErrorWhileExecute_itShouldThrowPDOException(): void
    {
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('bindValue')->willReturn(true);
        $statementMock->method('execute')->willReturn(false);
        $statementMock->method('errorInfo')->willReturn([1, 2, 'Error occured']);

        $pdoMock = $this->createMock(PDO::class);
        $pdoMock->method('prepare')->willReturn($statementMock);

        $this->expectException(PDOException::class);
        $this->expectExceptionCode(1);
        $this->expectExceptionMessage('Error occured');

        $model = new Model('test');
        $model->tableName = 'test';
        $model->setPdo($pdoMock);

        $model->delete(['id' => 100]);
    }

    /**
     * Test delete in execute is success then it should return the total of deleted data
     *
     * @return void
     */
    public function testDelete_inCaseExecuteIsSuccess_itShouldReturnTheTotalOfDeletedData(): void
    {
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('bindValue')->willReturn(true);
        $statementMock->method('execute')->willReturn(true);
        $statementMock->method('rowCount')->willReturn(1);

        $pdoMock = $this->createMock(PDO::class);
        $pdoMock->method('prepare')->willReturn($statementMock);

        $model = new Model('test');
        $model->tableName = 'test';
        $model->setPdo($pdoMock);

        $result = $model->delete(['id' => 100]);

        $this->assertEquals(1, $result);
    }
}