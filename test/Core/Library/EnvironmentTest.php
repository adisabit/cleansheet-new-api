<?php

use PHPUnit\Framework\TestCase;
use Skeleton\Core\Library\Environment;

class EnvironmentTest extends TestCase
{
    private $varName = 'TEST';
    private $value = 'Hello World!';
    private $defaultValue = 1000;

    /**
     * Before test
     *
     * @return void
     */
    protected function setUp(): void
    {
        putenv("$this->varName=$this->value");
    }

    /**
     * After test
     *
     * @return void
     */
    protected function tearDown(): void
    {
        putenv("$this->varName");    
    }

    /**
     * Test get in case environment variable is exist then it should return its value
     *
     * @return void
     */
    public function testGet_inCaseEnvironmentVariableIsExist_itShouldReturnItsValue(): void
    {
        $result = Environment::get($this->varName, $this->defaultValue);

        $this->assertEquals($this->value, $result);
    }

    /**
     * Test get in case environment variable is not exist then it should return its
     * default value
     *
     * @return void
     */
    public function testGet_inCaseEnvironmentVariableIsNotExist_itShouldReturnItsDefaultValue(): void
    {
        $result = Environment::get('NOT_EXIST_ENV_VAR', $this->defaultValue);

        $this->assertEquals($this->defaultValue, $result);
    }

    /**
     * Test get in case nested call and environment variable is exist then it should
     * return the value
     *
     * @return void
     */
    public function testGet_inCaseNestedCallAndEnvironmentVariableIsExist_itShouldReturnTheValue(): void
    {
        $result = Environment::get('NOT_EXIST_ENV_VAR', Environment::get($this->varName, $this->defaultValue));

        $this->assertEquals($this->value, $result);
    }

    /**
     * Test get in case nested call and environment variable is not exist then it should
     * return the default value
     *
     * @return void
     */
    public function testGet_inCaseNestedCallAndEnvironmentVariableIsNotExist_itShouldReturnTheDefaultValue(): void
    {
        $result = Environment::get('NOT_EXIST_ENV_VAR', Environment::get('ANOTHER_NOT_EXIST_ENV_VAR', $this->defaultValue));

        $this->assertEquals($this->defaultValue, $result);
    }
}