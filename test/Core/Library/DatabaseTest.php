<?php

namespace Skeleton\Test\Core\Library;

use PDO;
use PHPUnit\Framework\TestCase;
use Skeleton\Core\Exception\DatabaseConfigNotFoundException;
use Skeleton\Core\Library\Database;

class DatabaseTest extends TestCase
{
    /**
     * Setup
     *
     * @return void
     */
    protected function setUp(): void
    {
        \Skeleton\Core\Library\Database::$instances = [
            'test' => [
                'driver'    => \Skeleton\Core\Library\Environment::get('DB_TEST_DRIVER', 'mysql'),
                'host'      => \Skeleton\Core\Library\Environment::get('DB_TEST_HOST', 'localhost'),
                'port'      => \Skeleton\Core\Library\Environment::get('DB_TEST_PORT', 3306),
                'database'  => \Skeleton\Core\Library\Environment::get('DB_TEST_DATABASE', 'skeleton'),
                'username'  => \Skeleton\Core\Library\Environment::get('DB_TEST_USERNAME', 'skeleton'),
                'password'  => \Skeleton\Core\Library\Environment::get('DB_TEST_PASSWORD', 'skeleton'),
            ]
        ];
    }

    /**
     * Test init in case instance is not exist then it should throw DatabaseConfigurationNotExistExcaption
     *
     * @return void
     */
    public function testInit_inCaseInstanceNotExist_itShouldThrowDatabaseConfigurationNotExistException(): void
    {
        $databaseName = "invalid";

        $this->expectException(DatabaseConfigNotFoundException::class);
        $this->expectExceptionCode(500);
        $this->expectExceptionMessage("Database configuration for $databaseName is not exist!");

        Database::init($databaseName);
    }

    /**
     * Test init in case instance is exist then it should return PDO object
     *
     * @return void
     */
    public function testInit_inCaseInstanceIsExist_itShouldReturnPDO(): void
    {
        $pdo = Database::init('test');

        $this->assertInstanceOf(PDO::class, $pdo);
    }
}