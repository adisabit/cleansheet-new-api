<?php

namespace Skeleton\Test\Core\Exception;

use PHPUnit\Framework\TestCase;
use Skeleton\Core\Exception\DatabaseConfigNotFoundException;

class DatabaseConfigNotFoundExceptionTest extends TestCase
{
    /**
     * Test exception in case exception is occured then it should throw exception
     *
     * @return void
     */
    public function testException_inCaseExceptionIsOccured_itShouldThrowException(): void
    {
        $this->expectException(DatabaseConfigNotFoundException::class);
        $this->expectExceptionCode(500);
        $this->expectExceptionMessage('Database configuration for test is not exist!');

        throw new DatabaseConfigNotFoundException("Database configuration for test is not exist!");
    }   
}