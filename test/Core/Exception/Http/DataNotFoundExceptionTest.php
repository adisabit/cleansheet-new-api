<?php

namespace Skeleton\Test\Core\Exception;

use PHPUnit\Framework\TestCase;
use Skeleton\Core\Exception\Http\DataNotFoundException;

class DataNotFoundExceptionTest extends TestCase
{
    /**
     * Test exception in case exception is occured then it should throw exception
     *
     * @return void
     */
    public function testException_inCaseExceptionIsOccured_itShouldThrowException(): void
    {
        $this->expectException(DataNotFoundException::class);
        $this->expectExceptionCode(204);
        $this->expectExceptionMessage('Data not found!');

        throw new DataNotFoundException("Data not found!", 204);
    }   
}