ALTER TABLE order_service
	ADD IF NOT EXISTS order_ref VARCHAR(50) NOT NULL AFTER service_id,
	ADD IF NOT EXISTS order_date DATE NOT NULL AFTER order_ref;