CREATE TABLE IF NOT EXISTS `order_product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `delivery_date` date NOT NULL,
  `order_ref` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` decimal(20,2) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NEW' COMMENT 'NEW / PROCESSED / REJECTED / COMPLETED',
  `created_time` timestamp DEFAULT CURRENT_TIMESTAMP(),
  `updated_time` timestamp NULL ON UPDATE CURRENT_TIMESTAMP(),
  `deleted_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `order_product_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_product_id` bigint(20) NOT NULL,
  `type` varchar(10) not null comment 'INSERT / UPDATE / DELETE',
  `old_status` varchar(20) COLLATE utf8mb4_unicode_ci NULL COMMENT 'NEW / PROCESSED / REJECTED / COMPLETED',
  `new_status` varchar(20) COLLATE utf8mb4_unicode_ci NULL COMMENT 'NEW / PROCESSED / REJECTED / COMPLETED',
  `created_time` timestamp DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;