ALTER TABLE `order_product` 
    ADD IF NOT EXISTS `order_date` DATE DEFAULT NULL AFTER `delivery_date`;