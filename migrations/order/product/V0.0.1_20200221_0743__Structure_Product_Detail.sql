CREATE TABLE IF NOT EXISTS `order_product_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_product_id` bigint(20) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(20,2) NOT NULL,
  `amount` int(11) NOT NULL,
  `image_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_time` timestamp DEFAULT CURRENT_TIMESTAMP(),
  `updated_time` timestamp NULL ON UPDATE CURRENT_TIMESTAMP(),
  `deleted_time` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;