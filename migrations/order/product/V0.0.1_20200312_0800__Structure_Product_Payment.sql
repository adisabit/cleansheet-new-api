CREATE TABLE IF NOT EXISTS `order_product_payment` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_product_id` bigint(20) NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiry_time` datetime NOT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT NULL,
  `deleted_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_product_payment_order_product_id_IDX` (`order_product_id`) USING BTREE,
  KEY `order_product_payment_code_IDX` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `order_product_payment_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_product_payment_id` bigint(20) NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'INSERT / UPDATE / DELETE',
  `old_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_image_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_account_number` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_account_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_expiry_time` datetime DEFAULT NULL,
  `new_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_image_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_account_number` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_account_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_expiry_time` datetime DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_product_payment_history_order_product_payment_id_IDX` (`order_product_payment_id`) USING BTREE,
  KEY `order_product_payment_history_type_IDX` (`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;