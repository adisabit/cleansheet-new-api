DROP TABLE IF EXISTS `order_product_cart`;
CREATE TABLE IF NOT EXISTS `order_product_cart` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `status` varchar(15) NULL COMMENT 'SHIP / UPDATE / REMOVE / CHECKOUT', 
  `created_time` timestamp DEFAULT CURRENT_TIMESTAMP(),
  `updated_time` timestamp NULL ON UPDATE CURRENT_TIMESTAMP(),
  `deleted_time` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `order_product_cart_history`;
CREATE TABLE `order_product_cart_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_cart_id` bigint(20) NOT NULL,
  `type` varchar(10) NOT NULL COMMENT 'INSERT / UPDATE / DELETE',
  `old_amount` INT(10) unsigned NULL,
  `old_status` VARCHAR(15) NULL,
  `new_amount` INT(10) unsigned NULL,
  `new_status` VARCHAR(15) NULL,
  `created_time` timestamp DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;