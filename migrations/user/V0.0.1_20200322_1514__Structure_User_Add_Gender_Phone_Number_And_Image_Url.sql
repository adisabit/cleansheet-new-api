ALTER TABLE user
    ADD IF NOT EXISTS gender VARCHAR(20) DEFAULT NULL AFTER city,
    ADD IF NOT EXISTS phone_number VARCHAR(20) DEFAULT NULL AFTER gender,
    ADD IF NOT EXISTS image_url TEXT DEFAULT NULL AFTER phone_number,
    ADD IF NOT EXISTS date_of_birth DATE DEFAULT NULL AFTER image_url;

ALTER TABLE user_history
    ADD IF NOT EXISTS old_gender VARCHAR(20) DEFAULT NULL AFTER old_city,
    ADD IF NOT EXISTS old_phone_number VARCHAR(20) DEFAULT NULL AFTER old_gender,
    ADD IF NOT EXISTS old_image_url TEXT DEFAULT NULL AFTER old_phone_number,
    ADD IF NOT EXISTS old_date_of_birth DATE DEFAULT NULL AFTER old_image_url,
    ADD IF NOT EXISTS new_gender VARCHAR(20) DEFAULT NULL AFTER new_city,
    ADD IF NOT EXISTS new_phone_number varchar(20) DEFAULT NULL AFTER new_gender,
    ADD IF NOT EXISTS new_image_url TEXT DEFAULT NULL AFTER new_phone_number,
    ADD IF NOT EXISTS new_date_of_birth DATE DEFAULT NULL AFTER new_image_url;

UPDATE user
    SET image_url = 'https://gravatar.com/avatar/?size=200&d=identicon'
    WHERE image_url IS NULL;