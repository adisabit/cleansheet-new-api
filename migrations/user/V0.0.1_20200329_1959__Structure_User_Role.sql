CREATE TABLE IF NOT EXISTS user_role (
	id BIGINT UNSIGNED auto_increment NOT NULL,
	user_id BIGINT UNSIGNED NOT NULL,
	role_id BIGINT UNSIGNED NOT NULL,
	created_time datetime NULL,
	updated_time DATETIME NULL,
	deleted_time DATETIME NULL,
	CONSTRAINT user_role_PK PRIMARY KEY (id),
    INDEX user_role_user_id_IDX (user_id) USING BTREE,
    INDEX user_role_role_id_IDX (role_id) USING BTREE
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;

CREATE TABLE IF NOT EXISTS user_role_history (
	id BIGINT UNSIGNED auto_increment NOT NULL,
	user_role_id BIGINT UNSIGNED NOT NULL,
    type VARCHAR(10) COMMENT 'INSERT / UPDATE / DELETE',
	old_user_id BIGINT UNSIGNED DEFAULT NULL,
	old_role_id BIGINT UNSIGNED DEFAULT NULL,
	new_user_id BIGINT UNSIGNED DEFAULT NULL,
	new_role_id BIGINT UNSIGNED DEFAULT NULL,
	created_time datetime NULL,
	CONSTRAINT user_role_history_PK PRIMARY KEY (id),
    INDEX user_role_history_user_role_id_IDX (user_role_id) USING BTREE
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;