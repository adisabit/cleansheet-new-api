CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP(),
  `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(),
  `deleted_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `user_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'INSERT / UPDATE / DELETE',
  `old_name` varchar(100) COLLATE utf8mb4_unicode_ci NULL,
  `old_email` varchar(120) COLLATE utf8mb4_unicode_ci NULL,
  `old_city` varchar(100) COLLATE utf8mb4_unicode_ci NULL,
  `new_name` varchar(100) COLLATE utf8mb4_unicode_ci NULL,
  `new_email` varchar(120) COLLATE utf8mb4_unicode_ci NULL,
  `new_city` varchar(100) COLLATE utf8mb4_unicode_ci NULL,
  `created_time` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `user_auth` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forget_password_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verification_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT NULL,
  `deleted_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `user_auth_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_auth_id` bigint(20) NOT NULL,
  `type` varchar(10) NOT NULL COMMENT 'INSERT / UPDATE / DELETE',
  `old_password` text COLLATE utf8mb4_unicode_ci NULL,
  `old_salt` text COLLATE utf8mb4_unicode_ci NULL,
  `old_last_login_time` datetime NULL,
  `old_api_token` text COLLATE utf8mb4_unicode_ci NULL,
  `old_forget_password_token` text COLLATE utf8mb4_unicode_ci NULL,
  `old_email_verification_token` text COLLATE utf8mb4_unicode_ci NULL,
  `new_password` text COLLATE utf8mb4_unicode_ci NULL,
  `new_salt` text COLLATE utf8mb4_unicode_ci NULL,
  `new_last_login_time` datetime NULL,
  `new_api_token` text COLLATE utf8mb4_unicode_ci NULL,
  `new_forget_password_token` text COLLATE utf8mb4_unicode_ci NULL,
  `new_email_verification_token` text COLLATE utf8mb4_unicode_ci NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;