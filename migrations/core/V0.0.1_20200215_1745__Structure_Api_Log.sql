CREATE TABLE IF NOT EXISTS `api_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `base_url` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_header` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `request_time` datetime NOT NULL,
  `server_memory_usage` int(10) unsigned NOT NULL,
  `response_code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_time` datetime DEFAULT NULL,
  `error_type` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `error_code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `error_message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `error_trace` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;