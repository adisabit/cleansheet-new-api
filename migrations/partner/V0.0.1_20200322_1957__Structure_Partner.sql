CREATE TABLE IF NOT EXISTS `partner` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `description` text DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `deleted_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `partner_deleted_time_IDX` (`deleted_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `partner_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `partner_id` bigint(20) unsigned NOT NULL,
  `type` varchar(10) NOT NULL COMMENT 'INSERT / UPDATE / DELETE',
  `old_name` varchar(100) DEFAULT NULL,
  `old_city` varchar(50) DEFAULT NULL,
  `old_phone_number` varchar(20) DEFAULT NULL,
  `old_description` text DEFAULT NULL,
  `old_reason` text DEFAULT NULL,
  `new_name` varchar(100) DEFAULT NULL,
  `new_city` varchar(50) DEFAULT NULL,
  `new_phone_number` varchar(20) DEFAULT NULL,
  `new_description` text DEFAULT NULL,
  `new_reason` text DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `partner_history_partner_id_IDX` (`partner_id`) USING BTREE,
  KEY `partner_history_type_IDX` (`type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;