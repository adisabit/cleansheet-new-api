CREATE TABLE IF NOT EXISTS `registrant` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `nim` varchar(20) NOT NULL,
  `year` varchar(6) NOT NULL,
  `major` varchar(50) NOT NULL,
  `is_bidikmisi` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `address` text NOT NULL,
  `reason` text DEFAULT NULL,
  `image_url` text DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `deleted_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registrant_deleted_time_IDX` (`deleted_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `registrant_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `registrant_id` bigint(20) unsigned NOT NULL,
  `type` varchar(10) NOT NULL COMMENT 'INSERT / UPDATE / DELETE',
  `old_name` varchar(100) DEFAULT NULL,
  `old_phone_number` varchar(20) DEFAULT NULL,
  `old_nim` varchar(20) DEFAULT NULL,
  `old_year` varchar(6) DEFAULT NULL,
  `old_major` varchar(50) DEFAULT NULL,
  `old_is_bidikmisi` tinyint(3) unsigned DEFAULT NULL,
  `old_address` text DEFAULT NULL,
  `old_reason` text DEFAULT NULL,
  `old_image_url` text DEFAULT NULL,
  `new_name` varchar(100) DEFAULT NULL,
  `new_phone_number` varchar(20) DEFAULT NULL,
  `new_nim` varchar(20) DEFAULT NULL,
  `new_year` varchar(6) DEFAULT NULL,
  `new_major` varchar(50) DEFAULT NULL,
  `new_is_bidikmisi` tinyint(3) unsigned DEFAULT NULL,
  `new_address` text DEFAULT NULL,
  `new_reason` text DEFAULT NULL,
  `new_image_url` text DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `registrant_history_registrant_id_IDX` (`registrant_id`) USING BTREE,
  KEY `registrant_history_type_IDX` (`type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

UPDATE registrant
  SET image_url = 'https://gravatar.com/avatar/?size=200&d=identicon'
  WHERE image_url IS NULL;