CREATE TABLE IF NOT EXISTS `master_service` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'SANITATION / WASH_ITEM / ENVIRONMENT',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(20,2) NOT NULL,
  `unit` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estimation_time` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP(),
  `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(),
  `deleted_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `master_service_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` bigint(20) unsigned NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'INSERT / UPDATE / DELETE',
  `old_category` varchar(50) COLLATE utf8mb4_unicode_ci NULL COMMENT 'SANITATION / WASH_ITEM / ENVIRONMENT',
  `old_title` varchar(50) COLLATE utf8mb4_unicode_ci NULL,
  `old_description` text COLLATE utf8mb4_unicode_ci NULL,
  `old_price` decimal(20,2) NULL,
  `old_unit` varchar(10) COLLATE utf8mb4_unicode_ci NULL,
  `old_estimation_time` varchar(30) COLLATE utf8mb4_unicode_ci NULL,
  `old_image_url` text COLLATE utf8mb4_unicode_ci NULL,
  `new_category` varchar(50) COLLATE utf8mb4_unicode_ci NULL COMMENT 'SANITATION / WASH_ITEM / ENVIRONMENT',
  `new_title` varchar(50) COLLATE utf8mb4_unicode_ci NULL,
  `new_description` text COLLATE utf8mb4_unicode_ci NULL,
  `new_price` decimal(20,2) NULL,
  `new_unit` varchar(10) COLLATE utf8mb4_unicode_ci NULL,
  `new_estimation_time` varchar(30) COLLATE utf8mb4_unicode_ci NULL,
  `new_image_url` text COLLATE utf8mb4_unicode_ci NULL,
  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;