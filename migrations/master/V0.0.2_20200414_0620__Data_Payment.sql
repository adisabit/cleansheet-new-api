INSERT IGNORE INTO master_payment
    (id, code, name, description, account_number, account_name, image_url, created_time, updated_time, deleted_time)
VALUES
    (1, 'BANK_BRI', 'BRI', NULL, '0595-01-006179-53-3', 'Dihqon Nadaamist', 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/bank%2Fbri.png?alt=media&token=2e17f75e-d482-4f32-8445-53ab37736129', '2020-04-20 06:19:00.000', NULL, NULL),
    (2, 'BANK_BNI', 'BNI', NULL, '0849402944', 'M SIROJUL ABIDIN', 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/bank%2Fbni.png?alt=media&token=e6dd6d19-0fa7-474c-a6ac-6b27d19b8d06', '2020-04-20 06:19:00.000', NULL, NULL);
