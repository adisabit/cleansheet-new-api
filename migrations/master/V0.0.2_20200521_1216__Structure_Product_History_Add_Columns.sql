ALTER TABLE master_product_history
    ADD IF NOT EXISTS old_name varchar(50) DEFAULT NULL AFTER type,
    ADD IF NOT EXISTS new_name varchar(30) DEFAULT NULL AFTER old_image_url;