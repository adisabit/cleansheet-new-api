ALTER TABLE master_service MODIFY COLUMN IF EXISTS title varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;

UPDATE master_product SET `name` = 'Disinfektan Alami Kitosan 1lt' WHERE id = 6;
UPDATE master_service SET `price` = 15000 WHERE `id` = 3;
UPDATE master_service SET `unit` = '2x2 Meter' WHERE `id` = 4;
UPDATE master_service SET `estimation_time` = '30-60 Menit' WHERE id = 6;

INSERT IGNORE INTO master_product
    (id, name, price, image_url, created_time, updated_time, deleted_time)
VALUES
    (7, 'Alat Semprot Hand Sprayer 2L', 120000.00, 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/product%2FAlat%20Semprot%20Hand%20Sprayer%202%20L.jpg?alt=media&token=3f8d0887-b87b-4518-a9cf-4fa3598dc3d3', '2020-04-20 07:10:00.000', NULL, NULL),
    (8, 'Vacum Cleaner Wet and Dry', 1500000.00, 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/product%2FVacum%20Clenaer%20Wet%20and%20Dry.jpg?alt=media&token=6b6714fe-f54b-4a6f-8bac-3e8850e3148d', '2020-04-20 07:10:00.000', NULL, NULL),
    (9, 'Vacum Cleaner Extractor', 5000000.00, 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/product%2Fvacum%20Cleaner%20Extractor.jpg?alt=media&token=01e06c8f-ba20-4e72-8e8d-20ce09331f4d', '2020-04-20 07:10:00.000', NULL, NULL),
    (10, '3 in 1 Handy Vakum Tungau', 2000000.00, 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/product%2FVacum%20Cleaner3%20in%201%20Handy%20Vacum%20Tungau.jpg?alt=media&token=4bb407d1-340e-4d5c-bb04-00e3b66f2a5c', '2020-04-20 07:10:00.000', NULL, NULL);

INSERT IGNORE INTO master_service
    (id, category, title, description, price, unit, estimation_time, image_url, created_time, updated_time, deleted_time)
VALUES
    (7, 'WASH_ITEM', 'Cuci Sepatu', 'Membersihkan dan merawat sepatu menjadi baru lagi, dijamin bersih dan aman serta siap digunakan.', 50000.00, 'Sepatu', '1-2 Hari', 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/service%2FCuci%20Sepatu.jpg?alt=media&token=f1001218-c74c-45ef-83e9-7399d08bb2eb', '2020-04-20 07:53:00.000', NULL, NULL),
    (8, 'WASH_ITEM', 'Cuci Tas', 'Membersihkan dan merawat tas menjadi baru lagi, dijamin bersih dan aman serta siap digunakan.', 75000.00, 'Tas', '1-2 Hari', 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/service%2FCuci%20Tas.jpg?alt=media&token=35326b89-f336-4a14-b098-f31df1373252', '2020-04-20 07:53:00.000', NULL, NULL),
    (9, 'WASH_ITEM', 'Cuci Helm', 'Membersihkan helm dari kotoran dan bau. siap meningkatkan kegantengan dan kecantikan anda kembali ketika naik sepeda motor.', 30000.00, 'Helm', '1-2 Hari', 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/service%2FCuci%20Helm.jpg?alt=media&token=03aa5488-f2e5-412a-a954-9e7bc9e11c04', '2020-04-20 07:53:00.000', NULL, NULL),
    (10, 'ENVIRONMENT', 'Perapian dan Pembersihan Taman dan Halaman', 'Merapikan dan Membersihkan Halaman dan Taman anda menjadi lebih indah di pandang dengan sentuhan-sentuhan seni anak muda.', 15000.00, 'm2', '1 Bulan', 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/service%2Fperapian-halaman-rumah.jpeg?alt=media&token=06996eb7-cbcd-444a-b06a-07b9f32d9dfe', '2020-04-20 07:53:00.000', NULL, NULL),
    (11, 'SANITATION', 'Cuci Kursi', 'Membersihkan kursi dari noda dan kotoran sehingga tampak baru, bersih, dan nyaman digunakan.', 35000.00, 'Kursi', '1-2 Jam', 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/service%2FCuci%20Kursi.jpg?alt=media&token=96e00ea1-50a6-40de-b551-2c26d41c3093', '2020-04-20 07:53:00.000', NULL, NULL),
    (12, 'SANITATION', 'Cuci Bantal', 'Membersihkan bantal-bantal Anda di rumah, agar bersih seperti baru.', 35000.00, 'Bantal', '1-2 Jam', 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/service%2Fcuci%20bantal.jpg?alt=media&token=b8014603-9ad4-42bc-b386-6e94020f82ab', '2020-04-20 07:53:00.000', NULL, NULL),
    (13, 'SANITATION', 'General Cleaning (Rumah/Kantor/Gedung)', 'Membersihkan keseluruhan rumah/kantor/gedung meliputi kamar mandi, ruang utama dan kaca.', 5000.00, 'm2', '2-3 Jam', 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/service%2FGeneral%20Cleaning.jpg?alt=media&token=9f5565db-49fa-4fc1-8543-392b5cd720b7', '2020-04-20 07:53:00.000', NULL, NULL),
    (14, 'ENVIRONMENT', 'Pengelolaan Sampah Kekinian Komplek/Perumahan/Kampung', 'Mempermudah komplek/perumahan/kampung yang ingin mengelola sampahnya secara mandiri, sehingga lingkungan menjadi bersih, indah, hijau dan asri.', 50000.00, 'Rumah', '4 Bulan', 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/service%2FPengelolaan%20Sampah%20Kekinian%20Komplek.jpg?alt=media&token=a299a7bd-f308-4b0a-a532-524659bb0180', '2020-04-20 07:53:00.000', NULL, NULL);
