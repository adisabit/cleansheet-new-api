ALTER TABLE master_payment DROP KEY payment_code_IDX;

CREATE TABLE IF NOT EXISTS `master_payment_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `payment_id` bigint(20) unsigned NOT NULL,
  `type` varchar(10) NOT NULL COMMENT 'INSERT / UPDATE / DELETE',
  `old_code` varchar(30) DEFAULT NULL,
  `old_name` varchar(100) DEFAULT NULL,
  `old_description` text DEFAULT NULL,
  `old_account_number` varchar(30) DEFAULT NULL,
  `old_account_name` varchar(100) DEFAULT NULL,
  `old_image_url` text DEFAULT NULL,
  `new_code` varchar(30) DEFAULT NULL,
  `new_name` varchar(100) DEFAULT NULL,
  `new_description` text DEFAULT NULL,
  `new_account_number` varchar(30) DEFAULT NULL,
  `new_account_name` varchar(100) DEFAULT NULL,
  `new_image_url` text DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `payment_history_payment_id_IDX` (`payment_id`) USING BTREE,
  INDEX `payment_history_type_IDX` (`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;