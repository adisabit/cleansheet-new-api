INSERT IGNORE INTO master_product
    (id, name, price, image_url, created_time, updated_time, deleted_time)
VALUES
    (1, 'Karbol Cleaner 1lt', 20000.00, 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/product%2Fkarbol-cleaner-1lt.jpeg?alt=media&token=66464061-ea51-43ba-a1b2-a2aabd9cc105', '2020-04-13 20:06:00.000', NULL, NULL),
    (2, 'Parfum Sepatu 250ml', 40000.00, 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/product%2FParfum-sepatu-250ml.jpeg?alt=media&token=24e4ca9f-e164-4657-ab15-b1de057c75ba', '2020-04-13 20:06:00.000', NULL, NULL),
    (3, 'Hand Sanitizer 1lt', 100000.00, 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/product%2Fhandsanitizer-1lt.jpeg?alt=media&token=a7f39e25-3f00-4850-a670-e44e902a91b0', '2020-04-13 20:06:00.000', NULL, NULL),
    (4, 'Hand Sanitizer 250ml', 50000.00, 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/product%2Fhandsanitizer-250ml.jpeg?alt=media&token=da2faa10-9bd7-4c4c-871c-63264f0e2cf7', '2020-04-13 20:06:00.000', NULL, NULL),
    (5, 'Handwash 5lt', 60000.00, 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/product%2FHandwash-5lt.jpeg?alt=media&token=17482900-1e44-4b1d-9e38-6a9f3d99756d', '2020-04-13 20:06:00.000', NULL, NULL),
    (6, 'Disinfektan 1lt', 70000.00, 'https://firebasestorage.googleapis.com/v0/b/cleansheet-25aa3.appspot.com/o/product%2Fdisinfektan-1lt.jpeg?alt=media&token=cfd312a7-4f7d-4b1d-8f6c-2894e234b403', '2020-04-13 20:06:00.000', NULL, NULL);