ALTER TABLE master_service MODIFY COLUMN IF EXISTS unit varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;
ALTER TABLE master_service_history
    MODIFY COLUMN IF EXISTS old_unit varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL NULL,
    MODIFY COLUMN IF EXISTS new_unit varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL NULL;