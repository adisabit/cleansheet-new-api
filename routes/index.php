<?php

use Cleansheet\Config;
use Skeleton\Core\Exception\HttpException;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;
use Skeleton\Core\Library\Storage;

include __DIR__ . '/web/index.php';
include __DIR__ . '/admin/index.php';


Flight::route("GET /", function () {
    Response::success("CleanSheet API Server is online!");
});

Flight::route("POST /api/v1/image/upload", function () {
    $data = Request::getFiles();

    $image = file_get_contents($data->image['tmp_name']);
        
    $storage = new Storage(Config::$bucketName[Config::$environment]);

    $mimeWhiteList = ['image/png', 'image/jpg', 'image/jpeg'];

    $f = finfo_open();
    $mimeType = finfo_buffer($f, $image, FILEINFO_MIME_TYPE);

    $extension = explode('/', $mimeType)[1];
    $fileName = md5(microtime()) . '.' . $extension;

    if (!in_array($mimeType, $mimeWhiteList)) {
        throw new HttpException("Mime type tidak diizinkan!", 400);
    }

    $result = $storage->upload($image, $fileName);

    Response::success($result);
});

Flight::route("POST /api/v1/image/upload-base64", function () {
    $data = Request::getData();

    $image = $data->image_base64;

    $storage = new Storage(Config::$bucketName[Config::$environment]);

    $mimeWhiteList = ['image/png', 'image/jpg', 'image/jpeg'];
    $imgData = base64_decode($image);

    $f = finfo_open();
    $mimeType = finfo_buffer($f, $imgData, FILEINFO_MIME_TYPE);

    $extension = explode('/', $mimeType)[1];
    $fileName = md5(microtime()) . '.' . $extension;

    if (!in_array($mimeType, $mimeWhiteList)) {
        throw new HttpException("Mime type tidak diizinkan!", 400);
    }

    $result = $storage->uploadBase64($image, $fileName);

    Response::success($result);
});