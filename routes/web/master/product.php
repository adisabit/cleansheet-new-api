<?php

use Cleansheet\Master\Product\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/master/products";
Flight::route("GET $routePrefix", function() {    
    $queryString = Request::getQuery();    

    $product = new Api();
    $result = $product->getAllProducts($queryString->page ?? 1, $queryString->item_per_page);

    Response::success($result);
});
