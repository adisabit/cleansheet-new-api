<?php

use Cleansheet\Master\Payment\Api;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/master/payments";

Flight::route("GET $routePrefix", function() {    
    $service = new Api();
    $result = $service->getAllPayments();

    Response::success($result);
});
