<?php

use Cleansheet\Master\Service\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/master/services";

Flight::route("GET $routePrefix", function() {    
    $queryString = Request::getQuery();    
    $page = $queryString->page ?? 1;

    $service = new Api();
    $result = $service->getAllServices($page);

    Response::success($result);
});

Flight::route("GET $routePrefix/get-by-category", function() {    
    $queryString = Request::getQuery();

    $page = $queryString->page ?? 1;
    $category = $queryString->category;
    $itemPerPage = $queryString->item_per_page;

    $service = new Api();
    $result = $service->findByCategory($category, $page, $itemPerPage);

    Response::success($result);
});