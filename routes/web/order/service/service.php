<?php

use Cleansheet\Order\Service\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/order/service";

Flight::route("POST $routePrefix", function() {
    $userId = Flight::auth();
    $data = Request::getData();

    $orderServiceData = [
        "order_date"        => date("Y-m-d"),
        "user_id"           => $userId,
        "service_id"        => $data->service_id,
        "name"              => $data->name,
        "phone_number"      => $data->phone_number,
        "pickup_date"       => $data->pickup_date,
        "pickup_time"       => $data->pickup_time,
        "notes"             => $data->notes,
        "pickup_address"    => $data->pickup_address
    ];

    $orderService = new Api();
    $result = $orderService->create($orderServiceData);

    Response::success($result, 201);
});

Flight::route("GET $routePrefix/user", function() {
    $userId = Flight::auth();

    $orderService = new Api();
    $result = $orderService->findByUser($userId);

    Response::success($result);
});