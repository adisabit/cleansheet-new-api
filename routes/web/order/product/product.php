<?php

use Cleansheet\Order\Product\Api;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/order/product";

Flight::route("GET $routePrefix/user", function() {
    $userId = Flight::auth();

    $product = new Api();
    $result = $product->findByUser($userId);

    Response::success($result);
});