<?php

use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;
use Cleansheet\Order\Product\Payment\Api;

$routePrefix = "/api/v1/order/product/payment";

Flight::route("POST $routePrefix/pay", function() {
    $data = Request::getData();
    
    $paymentData = [
        'order_product_id'  => $data->order_product_id,
        'code'              => $data->code,
        'name'              => $data->name,
        'description'       => $data->description,
        'account_number'    => $data->account_number,
        'account_name'      => $data->account_name,
        'image_url'         => $data->image_url,
    ];

    $payment = new Api();
    $result = $payment->pay($paymentData);

    Response::success($result);
});