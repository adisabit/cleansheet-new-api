<?php

use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;
use Cleansheet\Order\Product\Cart\Api;

$routePrefix = "/api/v1/order/product/cart";

Flight::route("GET $routePrefix", function() {
    $userId = Flight::auth();
    
    $cart = new Api();
    $result = $cart->findCartData($userId);

    Response::success($result);
});

Flight::route("POST $routePrefix/ship", function() {
    $userId = Flight::auth();
    $data = Request::getData();

    $cart = new Api();
    $result = $cart->ship($data->product_id, $userId, $data->amount);

    Response::success($result, 201);
});

Flight::route("POST $routePrefix/remove", function() {
    $userId = Flight::auth();
    $data = Request::getData();

    $cart = new Api();
    $result = $cart->remove($data->product_id, $userId);

    Response::success($result);
});

Flight::route("POST $routePrefix/checkout", function() {
    $userId = Flight::auth();
    $data = Request::getData();

    $checkoutData = [
        "order_date"    => date("Y-m-d"),
        "delivery_date" => $data->delivery_date,
        "name"          => $data->name,
        "email"         => $data->email,
        "phone_number"  => $data->phone_number,
        "city"          => $data->city,
        "postal_code"   => $data->postal_code,
        "address"       => $data->address,
        "notes"         => $data->notes
    ];

    $cart = new Api();
    $result = $cart->checkout($userId, $checkoutData);

    Response::success($result);
});