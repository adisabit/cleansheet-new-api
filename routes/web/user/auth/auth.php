<?php

use Cleansheet\User\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/auth";

Flight::route("POST $routePrefix/register", function() {
    $data = Request::getData();
    
    $user = new Api();
    $result = $user->register($data->getData());

    Response::success($result, 201);
});

Flight::route("POST $routePrefix/login", function() {
    $data = Request::getData();
    
    $user = new Api();
    $result = $user->login($data->email, $data->password);

    Response::success($result);
});

Flight::route("POST $routePrefix/token/refresh", function() {
    $data = Request::getData();
    
    $user = new Api();
    $result = $user->refreshToken($data->access_token, $data->refresh_token);

    Response::success($result);
});
