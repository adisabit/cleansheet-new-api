<?php

use Cleansheet\User\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/user/profile";

Flight::route("GET $routePrefix", function() {
    $userId = Flight::auth();

    $profile = new Api();
    $result = $profile->getById($userId);

    Response::success($result);
});

Flight::route("POST $routePrefix/upload-profile-image", function() {
    Flight::auth();
    $data = Request::getData();

    $profile = new Api();
    $result = $profile->uploadProfileImage($data->image_base64);

    Response::success($result);
});

Flight::route("POST $routePrefix/update", function() {
    $userId = Flight::auth();
    $data = Request::getData();

    $profileData = [
        'name'          => $data->name,
        'email'         => $data->email,
        'gender'        => $data->gender,
        'phone_number'  => $data->phone_number,
        'image_url'     => $data->image_url,
        'date_of_birth' => $data->date_of_birth,
    ];

    $profile = new Api();
    $result = $profile->update($userId, $profileData);

    Response::success($result);
});