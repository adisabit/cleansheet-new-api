<?php

use Cleansheet\Registrant\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/registrant";

Flight::route("POST $routePrefix/create", function() {
    $data = Request::getData();
    
    $registrantData = [
        'name'          => $data->name,
        'phone_number'  => $data->phone_number,
        'nim'           => $data->nim,
        'year'          => $data->year,
        'major'         => $data->major,
        'is_bidikmisi'  => $data->is_bidikmisi,
        'address'       => $data->address,
        'reason'        => $data->reason,
    ];

    $registrant = new Api();
    $result = $registrant->create($registrantData);

    Response::success($result, 201);
});
