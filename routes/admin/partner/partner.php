<?php

use Cleansheet\Partner\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/admin/partner";

Flight::route("POST $routePrefix/create", function() {
    Flight::admin();
    
    $data = Request::getData();
    
    $partnerData = [
        'name'          => $data->name,
        'city'          => $data->city,
        'phone_number'  => $data->phone_number,
        'description'   => $data->description,
        'reason'        => $data->reason,
    ];

    $partner = new Api();
    $result = $partner->create($partnerData);

    Response::success($result, 201);
});

Flight::route("POST $routePrefix/update", function() {
    Flight::admin();
    
    $data = Request::getData();
    
    $partnerData = [
        'name'          => $data->name,
        'city'          => $data->city,
        'phone_number'  => $data->phone_number,
        'description'   => $data->description,
        'reason'        => $data->reason,
    ];

    $partner = new Api();
    $result = $partner->update($partnerData, ['id' => $data->id]);

    Response::success($result);
});

Flight::route("POST $routePrefix/delete", function() {
    Flight::admin();
    
    $data = Request::getData();
    
    $partner = new Api();
    $result = $partner->delete(['id' => $data->id]);

    Response::success($result);
});

Flight::route("GET $routePrefix/get", function() {
    Flight::admin();

    $data = Request::getQuery();

    $partner = new Api();
    $result = $partner->getById($data->id);

    Response::success($result);
});

Flight::route("GET $routePrefix/get-all", function() {
    Flight::admin();
    
    $queryString = Request::getQuery();

    $partner = new Api();
    $result = $partner->findPartners($queryString->page ?? 1, $queryString->item_per_page);

    Response::success($result);
});
