<?php

use Cleansheet\Order\Service\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/admin/order/service";

Flight::route("GET $routePrefix/get-all", function() {    
    Flight::admin();

    $query = Request::getQuery();

    $product = new Api();
    $result = $product->getAllServices($query->page ?? 1, $query->item_per_page);

    Response::success($result);
});

Flight::route("GET $routePrefix/get", function() {    
    Flight::admin();

    $query = Request::getQuery();

    $product = new Api();
    $result = $product->getById($query->id);

    Response::success($result);
});

Flight::route("POST $routePrefix/pay", function() {    
    Flight::admin();
    
    $data = Request::getData();

    $product = new Api();
    $result = $product->pay($data->id, $data->remark);

    Response::success($result);
});

Flight::route("POST $routePrefix/reject", function() {    
    Flight::admin();
    
    $data = Request::getData();

    $product = new Api();
    $result = $product->reject($data->id, $data->remark ?? '');

    Response::success($result);
});

Flight::route("POST $routePrefix/process", function() {    
    Flight::admin();
    
    $data = Request::getData();

    $product = new Api();
    $result = $product->process($data->id, $data->remark);

    Response::success($result);
});

Flight::route("POST $routePrefix/complete", function() {    
    Flight::admin();
    
    $data = Request::getData();

    $product = new Api();
    $result = $product->complete($data->id, $data->remark);

    Response::success($result);
});
