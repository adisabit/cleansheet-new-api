<?php

use Cleansheet\Master\Product\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/admin/master/product";

Flight::route("GET $routePrefix/get-all", function() {    
    Flight::admin();
    
    $data = Request::getQuery();

    $service = new Api();
    $result = $service->getAllProducts($data->page ?? 1, $data->item_per_page);

    Response::success($result);
});

Flight::route("GET $routePrefix/get", function() {    
    Flight::admin();
    
    $data = Request::getQuery();

    $service = new Api();
    $result = $service->getById($data->id);

    Response::success($result);
});

Flight::route("POST $routePrefix/create", function() {    
    Flight::admin();
    
    $data = Request::getData();

    $productData = [
        'name' => $data->name, 
        'price' => $data->price, 
        'image_url' => $data->image_url, 
    ];

    $service = new Api();
    $result = $service->create($productData);

    Response::success($result);
});

Flight::route("POST $routePrefix/update", function() {    
    Flight::admin();
    
    $data = Request::getData();

    $productData = [
        'name' => $data->name, 
        'price' => $data->price, 
        'image_url' => $data->image_url, 
    ];

    $service = new Api();
    $result = $service->update($productData, [ 'id' => $data->id ]);

    Response::success($result);
});

Flight::route("POST $routePrefix/delete", function() {    
    Flight::admin();
    
    $data = Request::getData();

    $service = new Api();
    $result = $service->delete([ 'id' => $data->id ]);

    Response::success($result);
});