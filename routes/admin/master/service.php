<?php

use Cleansheet\Master\Service\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/admin/master/service";

Flight::route("GET $routePrefix/get-all", function() {    
    Flight::admin();
    
    $data = Request::getQuery();

    $service = new Api();
    $result = $service->getAllServices($data->page ?? 1, $data->item_per_page);

    Response::success($result);
});

Flight::route("GET $routePrefix/get", function() {    
    Flight::admin();
    
    $data = Request::getQuery();

    $service = new Api();
    $result = $service->getById($data->id);

    Response::success($result);
});

Flight::route("POST $routePrefix/create", function() {    
    Flight::admin();
    
    $data = Request::getData();

    $serviceData = [
        'category' => $data->category, 
        'title' => $data->title, 
        'description' => $data->description, 
        'price' => $data->price, 
        'unit' => $data->unit, 
        'estimation_time' => $data->estimation_time, 
        'image_url' => $data->image_url, 
    ];

    $service = new Api();
    $result = $service->create($serviceData);

    Response::success($result);
});

Flight::route("POST $routePrefix/update", function() {    
    Flight::admin();
    
    $data = Request::getData();

    $serviceData = [
        'category' => $data->category, 
        'title' => $data->title, 
        'description' => $data->description, 
        'price' => $data->price, 
        'unit' => $data->unit, 
        'estimation_time' => $data->estimation_time, 
        'image_url' => $data->image_url, 
    ];

    $service = new Api();
    $result = $service->update($serviceData, [ 'id' => $data->id ]);

    Response::success($result);
});

Flight::route("POST $routePrefix/delete", function() {    
    Flight::admin();
    
    $data = Request::getData();

    $service = new Api();
    $result = $service->delete([ 'id' => $data->id ]);

    Response::success($result);
});