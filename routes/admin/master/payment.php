<?php

use Cleansheet\Master\Payment\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/admin/master/payment";

Flight::route("POST $routePrefix/create", function() {
    Flight::admin();

    $data = Request::getData();

    $paymentData = [
        'code' => $data->code,
        'name' => $data->name,
        'description' => $data->description,
        'account_number' => $data->account_number,
        'account_name' => $data->account_name,
        'image_url' => $data->image_url,
    ];

    $payment = new Api();
    $result = $payment->create($paymentData);

    Response::success($result);
});

Flight::route("POST $routePrefix/update", function() {    
    Flight::admin();

    $data = Request::getData();

    $paymentData = [
        'code' => $data->code,
        'name' => $data->name,
        'description' => $data->description,
        'account_number' => $data->account_number,
        'account_name' => $data->account_name,
        'image_url' => $data->image_url,
    ];

    $payment = new Api();
    $result = $payment->update($paymentData, [ 'id' => $data->id ]);

    Response::success($result);
});

Flight::route("POST $routePrefix/delete", function() { 
    Flight::admin();

    $data = Request::getData();

    $payment = new Api();
    $result = $payment->delete([ 'id' => $data->id ]);

    Response::success($result);
});

Flight::route("GET $routePrefix", function() {    
    Flight::admin();

    $payment = new Api();
    $result = $payment->getAllPayments();

    Response::success($result);
});

Flight::route("GET $routePrefix/get", function() {    
    Flight::admin();
    
    $data = Request::getQuery();

    $payment = new Api();
    $result = $payment->getById($data->id);

    Response::success($result);
});
