<?php

use Cleansheet\Registrant\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/admin/registrant";

Flight::route("POST $routePrefix/create", function() {
    Flight::admin();

    $data = Request::getData();
    
    $registrantData = [
        'name'          => $data->name,
        'phone_number'  => $data->phone_number,
        'nim'           => $data->nim,
        'year'          => $data->year,
        'major'         => $data->major,
        'is_bidikmisi'  => $data->is_bidikmisi,
        'address'       => $data->address,
        'reason'        => $data->reason,
    ];

    $registrant = new Api();
    $result = $registrant->create($registrantData);

    Response::success($result, 201);
});

Flight::route("POST $routePrefix/update", function() {
    Flight::admin();

    $data = Request::getData();

    $registrant = new Api();
    $result = $registrant->update($data->id, $data->data);

    Response::success($result);
});

Flight::route("POST $routePrefix/delete", function() {
    Flight::admin();

    $data = Request::getData();
    
    $registrant = new Api();
    $result = $registrant->delete($data->id);

    Response::success($result);
});

Flight::route("GET $routePrefix/get", function() {
    Flight::admin();

    $data = Request::getQuery();

    $registrant = new Api();
    $result = $registrant->getById($data->id);

    Response::success($result);
});

Flight::route("GET $routePrefix/get-all", function() {
    Flight::admin();
    
    $queryString = Request::getQuery();

    $registrant = new Api();
    $result = $registrant->findRegistrants($queryString->page ?? 1, $queryString->item_per_page);

    Response::success($result);
});
