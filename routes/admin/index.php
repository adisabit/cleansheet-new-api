<?php

// master module routes
include __DIR__ . '/master/index.php';

// order module routes
include __DIR__ . '/order/product/index.php';
include __DIR__ . '/order/service/index.php';

// partner module routes
include __DIR__ . '/partner/index.php';

// registrant module routes
include __DIR__ . '/registrant/index.php';

// metadata module routes
include __DIR__ . '/metadata/index.php';

// user module routes
include __DIR__ . '/user/index.php';