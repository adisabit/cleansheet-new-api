<?php

use Cleansheet\Metadata\Contact\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/admin/metadata/contact";
$contact = new Api();

/**
 * Get list of contact route
 */
Flight::route("GET $routePrefix", function () use ($contact) {
    Flight::admin();

    $result = $contact->list();

    Response::success($result);
});

/**
 * Get contact detail routes
 */
Flight::route("GET $routePrefix/@id", function ($id) use ($contact) {
    Flight::admin();

    $result = $contact->retrieve($id);

    Response::success($result);
});

/**
 * Create contact routes
 */
Flight::route("POST $routePrefix", function () use ($contact) {
    Flight::admin();

    $data = Request::getData();
    $result = $contact->create($data->getData());

    Response::success($result);
});

/**
 * Update contact routes
 */
Flight::route("PUT $routePrefix/@id", function ($id) use ($contact) {
    Flight::admin();

    $data = Request::getData();
    $result = $contact->update($data->getData(), ['id' => $id]);

    Response::success($result);
});

Flight::route("DELETE $routePrefix/@id", function ($id) use ($contact) {
    Flight::admin();

    $result = $contact->delete(['id' => $id]);

    Response::success($result);
});