<?php

use Cleansheet\User\Api;
use Skeleton\Core\Library\Request;
use Skeleton\Core\Library\Response;

$routePrefix = "/api/v1/admin/user";
$user = new Api();

/**
 * Get list of users route
 */
Flight::route("GET $routePrefix", function () use ($user) {
    Flight::admin();
    
    $queryString = Request::getQuery();

    $result = $user->list($queryString->page ?? 1, $queryString->item_per_page);

    Response::success($result);
});

/**
 * Get user detail route
 */
Flight::route("GET $routePrefix/@id", function ($id) use ($user) {
    Flight::admin();

    $result = $user->getById($id);

    Response::success($result);
});

/**
 * Create user route
 */
Flight::route("POST $routePrefix", function () use ($user) {
    Flight::admin();

    $data = Request::getData();

    $result = $user->register($data->getData());

    Response::success($result);
});

/**
 * Update user route
 */
Flight::route("PUT $routePrefix/@id", function ($id) use ($user) {
    Flight::admin();

    $data = Request::getData();

    $result = $user->update($id, $data->getData());

    Response::success($result);
});

/**
 * Get list of users route
 */
Flight::route("DELETE $routePrefix/@id", function ($id) use ($user) {
    Flight::admin();

    $result = $user->delete($id);

    Response::success($result);
});

/**
 * Upload profile image
 */
Flight::route("POST $routePrefix/upload-profile-image", function() use ($user) {
    Flight::admin();

    $data = Request::getData();
    $result = $user->uploadProfileImage($data->image_base64);

    Response::success($result);
});