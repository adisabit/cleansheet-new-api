<?php

use Cleansheet\Config;

// Cross-Origin Resource Sharing (CORS) Support
Flight::route('OPTIONS *', function () {
    header('HTTP/1.1 204 No Content');
    header('Access-Control-Allow-Origin: ' . (Config::$environment === 'production' ? 'cleansheet-api.herokuapp.com' : '*'));
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
    header('Access-Control-Allow-Headers: Content-Type, ' . implode(',', array_values(Config::$headers)));
    header('Content-Type: application/json');

    die();
});

Flight::route('GET *', function () {
    header('Access-Control-Allow-Origin: ' . (Config::$environment === 'production' ? 'cleansheet-api.herokuapp.com' : '*'));
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
    header('Access-Control-Allow-Headers: Content-Type, ' . implode(',', array_values(Config::$headers)));
    header('Content-Type: application/json');

    return TRUE;
});

Flight::route('POST *', function () {
    header('Access-Control-Allow-Origin: ' . (Config::$environment === 'production' ? 'cleansheet-api.herokuapp.com' : '*'));
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE');
    header('Access-Control-Allow-Headers: Content-Type, ' . implode(',', array_values(Config::$headers)));
    header('Content-Type: application/json');

    return TRUE;
});
