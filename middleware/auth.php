<?php

use Cleansheet\Master\Role\Model;
use Cleansheet\User\Role\Service;
use Skeleton\Core\Exception\HttpException;
use Skeleton\Core\Library\Token;

Flight::map('auth', function() {
    $bearerToken = Token::getBearerToken();

    Token::validate($bearerToken);
    $userId = Token::getClaim($bearerToken, 'id');

    return $userId;
});

Flight::map('admin', function() {
    $bearerToken = Token::getBearerToken();

    Token::validate($bearerToken);
    $userId = Token::getClaim($bearerToken, 'id');

    $userRoleService = new Service();
    
    $hasAdminRole = $userRoleService->hasRole($userId, Model::ROLE_ADMIN);

    if (!$hasAdminRole) {
        throw new HttpException('User tidak berhak mengakses resource dikarenakan bukan admin!', 403);
    }
    
    return true;
});