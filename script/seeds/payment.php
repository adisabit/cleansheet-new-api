<?php

use Cleansheet\Master\Config;
use Skeleton\Core\Library\Database;
use Cleansheet\Master\Payment\Model;
use Cleansheet\Master\Payment\Service;

// initiate faker
$faker = Faker\Factory::create();

// seed product
$tableName = (new Model())->tableName;
$db = new Database(Config::$databaseName);
$paymentPdo = $db->getInstance();
$paymentPdo->query("truncate table $tableName");

$payment = new Service();

$codes = [
    'MANDIRI_VA',
    'BRI_VA',
    'BCA_VA',
    'BNI_VA'
];

$bank = [
    'MANDIRI_VA'    => [
        'name'              => 'Mandiri Virtual Account',
        'description'       => 'Mudah dan terverifikasi otomatis',
        'account_number'    => $faker->bankAccountNumber,
        'account_name'      => $faker->name,
        'image_url'         => $faker->imageUrl()
    ],
    'BRI_VA'        => [
        'name'              => 'BRI Virtual Account',
        'description'       => 'Mudah dan terverifikasi otomatis',
        'account_number'    => $faker->bankAccountNumber,
        'account_name'      => $faker->name,
        'image_url'         => $faker->imageUrl()
    ],
    'BCA_VA'        => [
        'name'              => 'BCA Virtual Account',
        'description'       => 'Mudah dan terverifikasi otomatis',
        'account_number'    => $faker->bankAccountNumber,
        'account_name'      => $faker->name,
        'image_url'         => $faker->imageUrl()
    ],
    'BNI_VA'        => [
        'name'              => 'BNI Virtual Account',
        'description'       => 'Mudah dan terverifikasi otomatis',
        'account_number'    => $faker->bankAccountNumber,
        'account_name'      => $faker->name,
        'image_url'         => $faker->imageUrl()
    ],
];

printf("[%s] Seeding $tableName...\n", date("Y-m-d H:i:s"));
foreach ($codes as $code) {
    $payment->create([
        'code'              => $code,
        'name'              => $bank[$code]['name'],
        'description'       => $bank[$code]['description'],
        'account_number'    => $bank[$code]['account_number'],
        'account_name'      => $bank[$code]['account_name'],
        'image_url'         => $bank[$code]['image_url']
    ]);
}