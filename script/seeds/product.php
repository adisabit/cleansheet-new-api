<?php

use Cleansheet\Master\Config;
use Skeleton\Core\Library\Database;
use Cleansheet\Master\Product\Model;
use Cleansheet\Master\Product\Service;

// initiate faker
$faker = Faker\Factory::create();

// seed product
$tableName = (new Model())->tableName;
$db = new Database(Config::$databaseName);
$productPdo = $db->getInstance();
$productPdo->query("truncate table $tableName");

$product = new Service();

printf("[%s] Seeding $tableName...\n", date("Y-m-d H:i:s"));
for ($i = 0; $i < 10; $i++) {
    $product->create([
        'name'      => $faker->name,
        'price'     => $faker->randomNumber(5),
        'image_url' => $faker->imageUrl()
    ]);
}