<?php

use Cleansheet\Master\Config;
use Skeleton\Core\Library\Database;
use Cleansheet\Master\Service\Model;
use Cleansheet\Master\Service\Service;

// initiate faker
$faker = Faker\Factory::create();

// seed product
$tableName = (new Model())->tableName;
$db = new Database(Config::$databaseName);
$servicePdo = $db->getInstance();
$servicePdo->query("truncate table $tableName");

$service = new Service();

printf("[%s] Seeding $tableName...\n", date("Y-m-d H:i:s"));

$category = ['SANITATION', 'WASH_ITEM', 'ENVIRONMENT'];

$lorem = [
    'Lorem ipsum, dolor sit amet consectetur adipisicing elit. In, molestiae!',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab, adipisci!',
    'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem, minima.',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga, cum.',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, animi.',
    'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptate, nulla!',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Et, nesciunt.',
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium, omnis?',
];

for ($i = 0; $i < 10; $i++) {
    $service->create([
        'category'          => $category[rand(0, 2)],
        'title'             => $faker->name,
        'description'       => $lorem[rand(0, count($lorem) - 1)],
        'price'             => $faker->randomNumber(5),
        'unit'              => 'kg',
        'estimation_time'   => date("Y-m-d H:i:s"),
        'image_url'         => $faker->imageUrl()
    ]);
}