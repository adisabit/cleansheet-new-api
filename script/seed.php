<?php

include __DIR__ . '/../vendor/autoload.php';
include __DIR__ . '/../config/database.php';

// include the migration file
include __DIR__ . '/seeds/product.php';
include __DIR__ . '/seeds/service.php';
include __DIR__ . '/seeds/payment.php';